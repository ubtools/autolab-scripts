import csv
from selenium.webdriver.common.by import By


def collect_max_scores(driver, course, roster_emails, assignments):
    for assignment in assignments:
        logdata = []

        # Load grade sheet.
        driver.get('https://autograder.cse.buffalo.edu/courses/' + course + '/assessments/' + assignment + '/viewGradesheet')

        # Scroll down the page to force the entire table to render.
        for i in range(0, 200):
            driver.execute_script("window.scrollTo(0,document.body.scrollHeight);")
        # End for

        # Propagate the hidden table info by clicking on all email addresses.
        latest_submission_index = len(driver.find_elements(By.CSS_SELECTOR,'td.enumerator'))

        # Load popovers
        email_elements = driver.find_elements_by_class_name('email')
        # email_addresses = []
        for index, email_element in enumerate(email_elements):
            if index == latest_submission_index:
                break
            # End if
            driver.execute_script("arguments[0].scrollIntoView(true);", email_element)
            driver.execute_script("arguments[0].click();", email_element)

            # email_addresses.append(email_element.get_attribute('innerText').strip())
        # End for

        # Extract the page with all submissions from a single student and collect the links.
        submissions_table_rows = driver.find_elements_by_class_name('sub_info')
        filename_elements = driver.find_elements_by_class_name('filename')
        for index, submissions_row in enumerate(submissions_table_rows):
            email_address = filename_elements[index].get_attribute('innerText').strip()
            email_address = email_address[:email_address.find('_')]
            # Skip students no longer in the course and TA/instructor submissions.
            if email_address not in roster_emails:
                continue
            # if email_addresses[index] not in roster_emails or email_addresses[index] in gradebook_ids:
            #     continue
            # End if

            # Get link to student's submissions page.
            submission_link = str(submissions_row.find_element_by_tag_name('a').get_attribute('href')).strip()

            date_string = str(submissions_row.find_elements(By.TAG_NAME, 'td')[-5].get_attribute('innerText')).strip()

            # logdata.append([email_addresses[index], submission_link, date_string])
            logdata.append([email_address, submission_link, date_string])
        # End for

        for student_data in logdata:
            driver.get(student_data[1])

            # Collect the rows in the student submissions table.
            student_submissions_table_rows = driver.find_elements(By.CSS_SELECTOR, 'tr[style]')

            max_submission_date = ''
            max_submission_score = 0
            max_submission_index = 0
            latest_submission_index = len(student_submissions_table_rows) - 1
            for index, student_submission_row in enumerate(reversed(student_submissions_table_rows)):
                submission_score = 0
                row_elements = student_submission_row.find_elements(By.CSS_SELECTOR, 'td')

                # Extract per problem score and add to total.
                # -- float cast takes str -> number,  int truncates (whole values still end with .0).
                # -- remove int cast if fractional scores are allowed.
                date_element_text = row_elements[2].get_attribute('innerText')
                score_element_text = row_elements[3].get_attribute('innerText')
                if any(char.isdigit() for char in score_element_text):
                    submission_score = float(score_element_text)
                # End if

                # Record the score for the most recent submission made.
                if index == latest_submission_index:
                    latest_submission_score = submission_score
                # End if

                # Update max score for assignment.
                if submission_score > max_submission_score:
                    max_submission_date = date_element_text
                    max_submission_score = submission_score
                    max_submission_index = index
                # End if
            # End for
            student_data.append(max_submission_date)
            student_data.append(max_submission_score)
            student_data.append(max_submission_index)
            student_data.append(latest_submission_score)
            student_data.append(latest_submission_index)

        # End for

        # Write csv file containing modified students and also double charged students.
        with open(assignment + '-max-submissions.csv', 'w', newline='', encoding='ascii') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

            # Write header.
            spamwriter.writerow(['Email', 'Submissions Page', 'Latest Submission Date', 'Max Submission Date',  'Max Submission Score', 'Max Submission Index', 'Latest Submission Score', 'Latest Submission Index'])

            # Write log data.
            for output_array in logdata:
                spamwriter.writerow(output_array)
            # End for
        # End with
    # End for
# End def
