from selenium import webdriver
from compute_max import collect_max_scores
# from updateMax import update_max_scores
from regrade_all import regrade_all_submissions
from computeUnusedGracedays import collect_gdu_pld
from early_bonus import early_bonus, zero_tweaks
from submission_times import aggregate_latest_submission_times, aggregate_all_submission_times
from upload_files import upload_files
import csv
from datetime import date

import sys

if len(sys.argv) != 4:
    print('usage: python3 driver.py course chromedriver roster assignments')
    print('  course: name of course on autolab. E.g., cse250-f18')
    print('  chromedriver: path to chromedriver executable')
    print('  roster: path to course roster csv file. Can be downloaded from UBLearns using comma separator.')
    print('          expected format: "Last Name","First Name","Username","Student ID","Last Access","Availability"')
    print('  assignments: comma separated list of assignments (no spaces)')
    print('          assignment string can be obtained from url for assignment in Autolab')
    print('          E.g., https://autograder.cse.buffalo.edu/courses/cse250-f18/assessments/a1programming')
    print('                has assignment string a1programming')
    sys.exit()

# sys.argv[1] -- Autolab course.
course = sys.argv[1]

# sys.argv[2] -- path to chromedriver.
pathToChromedriver = sys.argv[2]
#  Load Chromedriver in incognito mode (so nothing is saved)
options = webdriver.ChromeOptions()
options.add_argument('--incognito')
driver = webdriver.Chrome(pathToChromedriver,0,options)

# Load Autolab
driver.get('https://ublearns.buffalo.edu/')

# Click login
loginButton = driver.find_element_by_class_name('login')
loginButton.click()

# Process roster while waiting for user to enter login credentials

# sys.argv[3] -- roster path
roster_path = sys.argv[3]

person_number_to_ubit_map = {}
ubit_map_to_person_number = {}
roster_emails = []
with open(roster_path,newline='') as roster_file:
    reader = csv.reader(roster_file, delimiter=',', quoting=csv.QUOTE_MINIMAL)

    # Skip header row.
    next(reader,None)

    for row in reader:
        # Map person number to ubit name.
        person_number_to_ubit_map[str(row[3]).strip()] = str(row[2]).strip()

        # Map ubit name to person number.
        ubit_map_to_person_number[str(row[2]).strip()] = str(row[3]).strip()

        # Create emails from roster.
        roster_emails.append(str(row[2]).strip()+'@buffalo.edu')

print('Loaded', len(roster_emails),'students.')

input('After logging into Autolab on Chrome webdriver, press enter...')

# Collect rubrics from current problem.



# Close browser
driver.close()
