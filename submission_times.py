# Used for date objects.
from datetime import date, timedelta
# Used for writing output.
import csv
from selenium.webdriver.common.by import By


def aggregate_latest_submission_times(driver, course, roster_emails, assignments):

    for assignment in assignments:
        logdata = []

        # Load grade sheet.
        driver.get('https://autograder.cse.buffalo.edu/courses/' + course + '/assessments/' + assignment + '/viewGradesheet')

        # Scroll down the page to force the entire table to render.
        for i in range(0, 200):
            driver.execute_script("window.scrollTo(0,document.body.scrollHeight);")
        # End for

        # Propagate the hidden table info by clicking on all email addresses.
        latest_submission_index = len(driver.find_elements(By.CSS_SELECTOR, 'td.enumerator'))

        # Load popovers
        email_elements = driver.find_elements_by_class_name('email')
        # email_addresses = []
        for index, email_element in enumerate(email_elements):
            if index == latest_submission_index:
                break
            # End if
            driver.execute_script("arguments[0].scrollIntoView(true);", email_element)
            driver.execute_script("arguments[0].click();", email_element)

            # email_addresses.append(email_element.get_attribute('innerText').strip())
        # End for

        # Extract the page with all submissions from a single student and collect the links.
        submissions_table_rows = driver.find_elements_by_class_name('sub_info')
        filename_elements = driver.find_elements_by_class_name('filename')
        for index, submissions_row in enumerate(submissions_table_rows):
            email_address = filename_elements[index].get_attribute('innerText').strip()
            email_address = email_address[:email_address.find('_')]
            # Skip students no longer in the course and TA/instructor submissions.
            if email_address not in roster_emails:
                continue
            # if email_addresses[index] not in roster_emails or email_addresses[index] in gradebook_ids:
            #     continue
            # End if

            # Get link to student's submissions page.
            submission_link = str(submissions_row.find_element_by_tag_name('a').get_attribute('href')).strip()

            date_string = str(
                submissions_row.find_elements(By.TAG_NAME, 'td')[-5].get_attribute('innerText')).strip()

            # logdata.append([email_addresses[index], submission_link, date_string])
            logdata.append([email_address, submission_link, date_string])
        # End for

        # Write csv file containing modified students and also double charged students.
        with open(assignment + '-latest-submissions.csv', 'w', newline='', encoding='ascii') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

            # Write header.
            spamwriter.writerow(['Email', 'Submissions Page', 'Latest Submission'])

            # Write log data.
            for output_array in logdata:
                spamwriter.writerow(output_array)
            # End for
        # End with
    # End for
# End def


def aggregate_all_submission_times(driver, course, roster_emails, assignment):
    logdata = []

    # Load grade sheet.
    driver.get('https://autograder.cse.buffalo.edu/courses/' + course + '/assessments/' + assignment + '/viewGradesheet')

    # Scroll down the page to force the entire table to render.
    for i in range(0, 200):
        driver.execute_script("window.scrollTo(0,document.body.scrollHeight);")
    # End for

    # Propagate the hidden table info by clicking on all email addresses.
    number_of_rows = len(driver.find_elements(By.CSS_SELECTOR,'td.enumerator'))

    # Load popovers
    email_elements = driver.find_elements_by_class_name('email')
    email_addresses = []
    for index, email_element in enumerate(email_elements):
        if index == number_of_rows:
            break
        # End if
        driver.execute_script("arguments[0].click();", email_element)
        driver.execute_script("arguments[0].scrollIntoView(true);", email_element)

        email_addresses.append(email_element.get_attribute('innerText').strip())
    # End for

    # Extract the page with all submissions from a single student and collect the links.
    submissions_links = driver.find_elements(By.CSS_SELECTOR, '.version a')

    student_submission_links = {}
    for index, submissions_link_element in enumerate(submissions_links):
        # Skip students no longer in the course and TA/instructor submissions.
        email_address = email_addresses[index]
        if email_address not in roster_emails:
            continue
        # End if

        # Get link to student's submissions page.
        submission_link = str(submissions_link_element.get_attribute('href')).strip()

        student_submission_links[email_address] = submission_link
    # End for

    for email_address, submission_link in student_submission_links.items():
        driver.get(submission_link)
        submissions_table_rows = driver.find_elements(By.CSS_SELECTOR, 'tbody tr')

        number_of_submissions = len(submissions_table_rows)
        for index, submission_row in enumerate(submissions_table_rows):
            submission_date = submission_row.find_elements(By.CSS_SELECTOR,'td.smallText')[0].get_attribute('innerText').strip()
            logdata.append([email_address,str(number_of_submissions - index), submission_date])
        # End for
    # End for

    # Write csv file containing modified students and also double charged students.
    with open(assignment + '-all-submissions.csv', 'w', newline='', encoding='ascii') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        # Write header.
        spamwriter.writerow(['Email', 'Submission #', 'Submission Date'])

        # Write log data.
        for output_array in logdata:
            spamwriter.writerow(output_array)
        # End for
    # End with
# End def