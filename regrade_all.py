import time
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains


def regrade_all_submissions(driver, course, assignment, roster_emails, delay_amount_inc, max_score):
    # Load grade sheet.
    driver.get(
        'https://autograder.cse.buffalo.edu/courses/' + course + '/assessments/' + assignment + '/viewGradesheet')

    # Scroll down the page to force the entire table to render.
    for i in range(0, 200):
        driver.execute_script("window.scrollTo(0,document.body.scrollHeight);")
    # End for

    # Propagate the hidden table info by clicking on all email_address addresses.
    # submissions_table_rows = driver.find_element_by_id('grades').find_elements_by_class_name('sorting_1')
    submissions_table_rows = driver.find_elements(By.CSS_SELECTOR, 'td.id.sorting_1 > a.email')
    for email_element in submissions_table_rows:
        # element = student_row_element.find_elements_by_tag_name('a')[0]
        # driver.execute_script("arguments[0].scrollIntoView(true);", element)
        # element.click()
        # action = ActionChains(driver)
        # action.move_to_element(email_element).click().perform()
        driver.execute_script("arguments[0].scrollIntoView(true);", email_element)
        email_element.click()
    # End for
    time.sleep(1)
    # Collect the links to each student's submissions list.
    # Page contents have changed. Reload table rows.
    # submissions_table_rows = driver.find_element_by_id('grades').find_elements_by_class_name('sorting_1')
    submissions_table_rows = driver.find_elements(By.CSS_SELECTOR, 'td.id.sorting_1 > a.email')
    # row_popover_links = driver.find_elements(By.CSS_SELECTOR, 'td.id.sorting_1 > div.popover td.version > a')
    row_popover_links = driver.find_elements(By.CSS_SELECTOR, 'div.popover td.version > a')
    if len(submissions_table_rows) != len(row_popover_links):
        print(f"ERROR: len(submissions_table_rows) = {len(submissions_table_rows)} != {len(row_popover_links)} = len(row_popover_links)")
        exit(1)
    submissions_links = []
    students_submission_info = {}
    for student_row_element,row_link_element in zip(submissions_table_rows,row_popover_links):
        # Extract the email_address address for the student.
        # email_address = str(student_row_element.find_element_by_class_name('email_address').get_attribute('innerText')).strip()
        email_address = str(student_row_element.get_attribute('innerText')).strip()
        # Skip if student is not enrolled in the course.
        if email_address not in roster_emails:
            continue

        # Extract the link for the page with all submissions from a single student.
        # link = str(student_row_element.find_elements_by_tag_name('a')[5].get_attribute('href')).strip()
        link = str(row_link_element.get_attribute('href')).strip()
        # Record link to submissions page and initialize entry for submission info.
        submissions_links.append(link)
        students_submission_info[email_address] = {'submissionLink': link,
                                                  'maxScoreSubmission': 0,
                                                  'maxScoreSubmissionNumber': 9999999}
    # End for

    # Visit each student's submissions page.
    for email_address, submission_info in students_submission_info.items():
        driver.get(submission_info['submissionLink'])

        # Determine number of submissions made by student.
        # student_submissions_table_rows = \
        #     driver.find_element_by_class_name('highlight') \
        #         .find_element_by_tag_name('tbody') \
        #         .find_elements_by_tag_name('tr')
        student_submissions_table_rows = driver.find_elements(By.CSS_SELECTOR, 'table.highlight > tbody > tr')
        number_of_submissions = len(student_submissions_table_rows)

        latest_submission = True
        for student_submission_row in student_submissions_table_rows:
            submission_score = 0
            submission_number = number_of_submissions

            for score_element in student_submission_row.find_elements_by_class_name('prettyBorder'):
                # Extract per problem score and add to total.
                # -- float cast takes str -> number,  int truncates (whole values still end with .0).
                # -- remove int cast if fractional scores are allowed.
                score_element_text = score_element.get_attribute('innerText')
                if any(char.isdigit() for char in score_element_text):
                    submission_score += float(score_element_text)

            # End for

            # Record the score for the most recent submission made.
            if latest_submission:
                latest_submission_score = submission_score
                latest_submission = False
            # End if

            # Update max score for assignment.
            if submission_score > submission_info['maxScoreSubmission']:
                submission_info['maxScoreSubmission'] = submission_score
            # End if

            # Record index of earliest submission to achieve max score.
            if submission_score == submission_info['maxScoreSubmission']:
                submission_info['maxScoreSubmissionNumber'] = submission_number
            # End if

            submission_number -= 1
        # End for

        if submission_info['maxScoreSubmission'] < max_score:
            # Regrade every submission of the student if they didn't achieve full points.
            delay_amount = delay_amount_inc
            for link_index in range(0, number_of_submissions):
                # Get rows each time through due to page refresh invalidating old elements.
                # -- when the page refreshes it invalidates previously obtained rows (not ideal).
                # -- only need this if regrading.
                # student_submissions_table_rows = \
                #     driver.find_element_by_class_name('highlight')\
                #         .find_element_by_tag_name('tbody')\
                #         .find_elements_by_tag_name('tr')
                student_submissions_table_rows = driver.find_elements(By.CSS_SELECTOR, 'table.highlight > tbody > tr')
                student_submission_row = student_submissions_table_rows[link_index]

                # Increment delay 10 seconds per request before sending next student's submissions to grader.
                # -- Recommended for longer running grading jobs.
                delay_amount = delay_amount + 2

                # Click regrade link.
                regrade_link_element = student_submission_row.find_elements_by_tag_name('a')[-3]
                if str(regrade_link_element.get_attribute('innerText')) == '(Regrade)':
                    regrade_link_element.click()
                else:
                    print(f'ERROR: couldn\'t find regrade link for {email_address} submission number {link_index}')
                # End if
            # End for

            # Pause between students to avoid overloading the grading queue.
            time.sleep(delay_amount)
        else:
            print(f'INFO:  Skipping {email_address} -- score is already max.')
        # End if
    # End for
# End regrade_all_submissions

def other_regrade_all_submissions(driver, course, assignment, roster_emails, delay_amount_inc, max_score):
    # Load grade sheet.
    driver.get(
        'https://autograder.cse.buffalo.edu/courses/' + course + '/assessments/' + assignment + '/viewGradesheet')

    # Scroll down the page to force the entire table to render.
    for i in range(0, 200):
        driver.execute_script("window.scrollTo(0,document.body.scrollHeight);")
    # End for

    # Propagate the hidden table info by clicking on all email addresses.
    number_of_rows = len(driver.find_elements(By.CSS_SELECTOR, 'td.enumerator'))

    # Load popovers
    email_elements = driver.find_elements_by_class_name('email')
    email_addresses = []
    for index, email_element in enumerate(email_elements):
        if index == number_of_rows:
            break
        # End if
        driver.execute_script("arguments[0].scrollIntoView(true);", email_element)
        driver.execute_script("arguments[0].click();", email_element)

        email_addresses.append(email_element.get_attribute('innerText').strip())
    # End for

    # Collect the links to each student's submissions list.
    submissions_table_rows = driver.find_element_by_id('grades').find_elements_by_class_name('sorting_1')
    submissions_links = []
    students_submission_info = {}
    for submissions_row in submissions_table_rows:
        # Extract the email address for the student.
        email_address = str(submissions_row.find_element_by_class_name('email').get_attribute('innerText')).strip()
        # Skip if student is not enrolled in the course.
        if email_address not in roster_emails:
            continue

        # Extract the link for the page with all submissions from a single student.
        a_tags = submissions_row.find_elements_by_tag_name('a')
        if len(a_tags) < 6:
            continue
        link = str(a_tags[5].get_attribute('href')).strip()
        # Record link to submissions page and initialize entry for submission info.
        submissions_links.append(link)
        students_submission_info[email_address] = {'submissionLink': link,
                                                  'maxScoreSubmission': 0,
                                                  'maxScoreSubmissionNumber': 9999999}
    # End for

    # Visit each student's submissions page.
    for email, submission_info in students_submission_info.items():
        driver.get(submission_info['submissionLink'])

        # Determine number of submissions made by student.
        student_submissions_table_rows = \
            driver.find_element_by_class_name('highlight') \
                .find_element_by_tag_name('tbody') \
                .find_elements_by_tag_name('tr')
        number_of_submissions = len(student_submissions_table_rows)

        latest_submission = True
        for student_submission_row in student_submissions_table_rows:
            submission_score = 0
            submission_number = number_of_submissions

            for score_element in student_submission_row.find_elements_by_class_name('prettyBorder'):
                # Extract per problem score and add to total.
                # -- float cast takes str -> number,  int truncates (whole values still end with .0).
                # -- remove int cast if fractional scores are allowed.
                score_element_text = score_element.get_attribute('innerText')
                if any(char.isdigit() for char in score_element_text):
                    submission_score += int(float(score_element_text))

            # End for

            # Record the score for the most recent submission made.
            if latest_submission:
                latest_submission_score = submission_score
                latest_submission = False
            # End if

            # Update max score for assignment.
            if submission_score > submission_info['maxScoreSubmission']:
                submission_info['maxScoreSubmission'] = submission_score
            # End if

            # Record index of earliest submission to achieve max score.
            if submission_score == submission_info['maxScoreSubmission']:
                submission_info['maxScoreSubmissionNumber'] = submission_number
            # End if

            submission_number -= 1
        # End for

        if submission_info['maxScoreSubmission'] < max_score:
            # Regrade every submission of the student if they didn't achieve full points.
            delay_amount = delay_amount_inc
            for link_index in range(1, number_of_submissions):
                # Get rows each time through.
                # -- when the page refreshes it invalidates previously obtained rows (not ideal).
                # -- only need this if regrading.
                student_submissions_table_rows = \
                    driver.find_element_by_class_name('highlight')\
                        .find_element_by_tag_name('tbody')\
                        .find_elements_by_tag_name('tr')
                student_submission_row = student_submissions_table_rows[link_index]

                # Increment delay 10 seconds per request before sending next student's submissions to grader.
                # -- Recommended for longer running grading jobs.
                delay_amount = delay_amount + 5

                # Click regrade link.
                regrade_link_element = student_submission_row.find_elements_by_tag_name('a')[-3]
                if str(regrade_link_element.get_attribute('innerText')) == '(Regrade)':
                    regrade_link_element.click()
                    # break
                else:
                    print('Error finding regrade link')
                # End if
            # End for

            # Pause between students to avoid overloading the grading queue.
            # time.sleep(delay_amount)
        else:
            print('Skipping ' + email + ' due to score.')
        # End if
    # End for
# End other_regrade_all_submissions