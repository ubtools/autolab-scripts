from selenium.webdriver.common.keys import Keys

def update_max_scores(driver, assignment):
    # Regrade pages
    driver.get('https://autograder.cse.buffalo.edu/courses/cse111-s18/assessments/' + assignment + '/viewGradesheet')

    currentEnrollment = ["nancyaco@buffalo.edu", "bademola@buffalo.edu", "tolaader@buffalo.edu", "shariara@buffalo.edu", "trallen4@buffalo.edu", "fnallote@buffalo.edu", "giannaam@buffalo.edu", "murrayan@buffalo.edu", "silvioan@buffalo.edu", "islamcha@buffalo.edu", "bakayoko@buffalo.edu", "mahadbeh@buffalo.edu", "jacobbet@buffalo.edu", "tylerbon@buffalo.edu", "essenceb@buffalo.edu", "bmbroadd@buffalo.edu", "chareabr@buffalo.edu", "ajbruni2@buffalo.edu", "blakebue@buffalo.edu", "dallasbu@buffalo.edu", "alexcari@buffalo.edu", "djcarlin@buffalo.edu", "brycecar@buffalo.edu", "kchen87@buffalo.edu", "rickyche@buffalo.edu", "niphapon@buffalo.edu", "tajwarch@buffalo.edu", "achu7@buffalo.edu", "sfcloth@buffalo.edu", "deancont@buffalo.edu", "scouliba@buffalo.edu", "alyssacu@buffalo.edu", "jaredc@buffalo.edu", "annadann@buffalo.edu", "deckert@buffalo.edu", "ryandian@buffalo.edu", "chaddiaz@buffalo.edu", "shaoshua@buffalo.edu", "ajdisist@buffalo.edu", "madispen@buffalo.edu", "adumpson@buffalo.edu", "onajeech@buffalo.edu", "jgegnoto@buffalo.edu", "rmeisenm@buffalo.edu", "tnfalzon@buffalo.edu", "sfranch@buffalo.edu", "marygala@buffalo.edu", "jpgardne@buffalo.edu", "ayeshaga@buffalo.edu", "seanedwa@buffalo.edu", "jakegeog@buffalo.edu", "nmgeorge@buffalo.edu", "paologer@buffalo.edu", "jongioni@buffalo.edu", "naomiwoo@buffalo.edu", "bmgould@buffalo.edu", "asengrad@buffalo.edu", "alexagra@buffalo.edu", "awgray@buffalo.edu", "angroege@buffalo.edu", "jagross2@buffalo.edu", "regruber@buffalo.edu", "agugliel@buffalo.edu", "yeyueguo@buffalo.edu", "rudolphh@buffalo.edu", "nahender@buffalo.edu", "carahoan@buffalo.edu", "johnhueb@buffalo.edu", "shanehy@buffalo.edu", "cmjeanpi@buffalo.edu", "jj246@buffalo.edu", "anishaka@buffalo.edu", "atkeirsb@buffalo.edu", "amkendri@buffalo.edu", "ak234@buffalo.edu", "ciarakie@buffalo.edu", "jacobkli@buffalo.edu", "sakruger@buffalo.edu", "mckuhn@buffalo.edu", "alexisku@buffalo.edu", "priyankm@buffalo.edu", "adrianla@buffalo.edu", "jlanfran@buffalo.edu", "jaehyukl@buffalo.edu", "jlevy7@buffalo.edu", "ali47@buffalo.edu", "elvisli@buffalo.edu", "shuoling@buffalo.edu", "wliu44@buffalo.edu", "carlyluk@buffalo.edu", "erikaluk@buffalo.edu", "xiaoyalu@buffalo.edu", "bsmakosy@buffalo.edu", "juliomal@buffalo.edu", "mzmarte@buffalo.edu", "martinov@buffalo.edu", "mfmcderm@buffalo.edu", "cmmcguir@buffalo.edu", "amm233@buffalo.edu", "hopemele@buffalo.edu", "kamranmu@buffalo.edu", "ajmullen@buffalo.edu", "mneurohr@buffalo.edu", "kellieno@buffalo.edu", "hpastern@buffalo.edu", "jayanpat@buffalo.edu", "jpiazza3@buffalo.edu", "pignato@buffalo.edu", "njplumle@buffalo.edu", "kevinpop@buffalo.edu", "blakepri@buffalo.edu", "erinprou@buffalo.edu", "denapugl@buffalo.edu", "mathieur@buffalo.edu", "nrainwat@buffalo.edu", "tysunrei@buffalo.edu", "zimingre@buffalo.edu", "jtrubens@buffalo.edu", "jmsalaza@buffalo.edu", "mschulwi@buffalo.edu", "dpschwar@buffalo.edu", "alizesco@buffalo.edu", "cscott3@buffalo.edu", "smeensha@buffalo.edu", "sharma96@buffalo.edu", "kokishid@buffalo.edu", "mnshishk@buffalo.edu", "davidsig@buffalo.edu", "salihahs@buffalo.edu", "suneetsi@buffalo.edu", "jsmehlik@buffalo.edu", "nstefani@buffalo.edu", "jas42@buffalo.edu", "nsurra@buffalo.edu", "hengta@buffalo.edu", "jiayetan@buffalo.edu", "gctaylor@buffalo.edu", "tiniyata@buffalo.edu", "willterr@buffalo.edu", "sarahtit@buffalo.edu", "noahtown@buffalo.edu", "reesetro@buffalo.edu", "jbtwomey@buffalo.edu", "ajwalter@buffalo.edu", "jacklynw@buffalo.edu", "rwang29@buffalo.edu", "xueweiwa@buffalo.edu", "mitchelw@buffalo.edu", "dariawat@buffalo.edu", "clweber@buffalo.edu", "xiaohanw@buffalo.edu", "emmawest@buffalo.edu", "chasewil@buffalo.edu", "laurynwi@buffalo.edu", "rjw26@buffalo.edu", "roxanewu@buffalo.edu", "renjiexi@buffalo.edu", "sedahriy@buffalo.edu", "alfredzh@buffalo.edu", "zhang77@buffalo.edu", "azhao5@buffalo.edu", "carleyzo@buffalo.edu", "mkoch4@buffalo.edu"]
    # currentEnrollment = {'amitblon@buffalo.edu': True, 'owentorr@buffalo.edu': True, 'igorkuzm@buffalo.edu': True,
    #                      'sneupane@buffalo.edu': True, 'sanchitb@buffalo.edu': True, 'daviddep@buffalo.edu': True,
    #                      'aadair@buffalo.edu': True, 'minxiang@buffalo.edu': True, 'tmsherwo@buffalo.edu': True,
    #                      'njkobis@buffalo.edu': True, 'hdtran@buffalo.edu': True, 'japettro@buffalo.edu': True,
    #                      'lalopezc@buffalo.edu': True, 'wenqianz@buffalo.edu': True, 'xliu72@buffalo.edu': True,
    #                      'wptimkey@buffalo.edu': True, 'javieryu@buffalo.edu': True, 'hansbas@buffalo.edu': True,
    #                      'ethanarm@buffalo.edu': True, 'seanchiu@buffalo.edu': True, 'dh33@buffalo.edu': True,
    #                      'dmconnol@buffalo.edu': True, 'aismail2@buffalo.edu': True, 'jmsiegel@buffalo.edu': True,
    #                      'bzheng2@buffalo.edu': True, 'wwong9@buffalo.edu': True, 'peterklo@buffalo.edu': True,
    #                      'jasjeeva@buffalo.edu': True, 'canyao@buffalo.edu': True, 'lucasbei@buffalo.edu': True,
    #                      'dtdistef@buffalo.edu': True, 'coreyrop@buffalo.edu': True, 'asoni@buffalo.edu': True,
    #                      'joseroma@buffalo.edu': True, 'aelazar@buffalo.edu': True, 'wpritcha@buffalo.edu': True,
    #                      'ajshapir@buffalo.edu': True, 'likhithr@buffalo.edu': True, 'ericbusc@buffalo.edu': True,
    #                      'zjross@buffalo.edu': True, 'nshaffer@buffalo.edu': True, 'pwelch@buffalo.edu': True,
    #                      'tbchase@buffalo.edu': True, 'dhknight@buffalo.edu': True, 'briantac@buffalo.edu': True,
    #                      'awoloszy@buffalo.edu': True, 'sfung3@buffalo.edu': True, 'cnallapa@buffalo.edu': True,
    #                      'sayefiqb@buffalo.edu': True, 'miaencar@buffalo.edu': True, 'gsilver@buffalo.edu': True,
    #                      'brconomo@buffalo.edu': True, 'yzou8@buffalo.edu': True, 'dsager@buffalo.edu': True,
    #                      'edwinchi@buffalo.edu': True, 'annaspri@buffalo.edu': True, 'sriniket@buffalo.edu': True,
    #                      'snmackay@buffalo.edu': True, 'pururval@buffalo.edu': True, 'asifhasa@buffalo.edu': True,
    #                      'jthejna@buffalo.edu': True, 'zhenduol@buffalo.edu': True, 'zatenenb@buffalo.edu': True,
    #                      'dmbrant@buffalo.edu': True, 'jeh24@buffalo.edu': True, 'jjlabont@buffalo.edu': True,
    #                      'garyyeun@buffalo.edu': True, 'alin29@buffalo.edu': True, 'ethansac@buffalo.edu': True,
    #                      'eykim5@buffalo.edu': True, 'jjpearce@buffalo.edu': True, 'cmsaless@buffalo.edu': True,
    #                      'mjlong2@buffalo.edu': True, 'jpcole2@buffalo.edu': True, 'mschowdh@buffalo.edu': True,
    #                      'jpmiller@buffalo.edu': True, 'rickywen@buffalo.edu': True, 'mklein5@buffalo.edu': True,
    #                      'manmeets@buffalo.edu': True, 'mamincon@buffalo.edu': True, 'piwaszko@buffalo.edu': True,
    #                      'iankamin@buffalo.edu': True, 'thharian@buffalo.edu': True, 'dli37@buffalo.edu': True,
    #                      'iwu@buffalo.edu': True, 'jchen293@buffalo.edu': True, 'korykais@buffalo.edu': True,
    #                      'lscarrol@buffalo.edu': True, 'jhuang42@buffalo.edu': True, 'bailytro@buffalo.edu': True,
    #                      'bmaul@buffalo.edu': True, 'lawzeeml@buffalo.edu': True, 'brianant@buffalo.edu': True,
    #                      'cmmuya@buffalo.edu': True, 'tonylin@buffalo.edu': True, 'aaronamo@buffalo.edu': True,
    #                      'dylandeg@buffalo.edu': True, 'vickykum@buffalo.edu': True, 'jzheng35@buffalo.edu': True,
    #                      'wedyer@buffalo.edu': True, 'jinkyuhw@buffalo.edu': True, 'rsmith24@buffalo.edu': True,
    #                      'renarodr@buffalo.edu': True, 'emilyrye@buffalo.edu': True, 'brianpra@buffalo.edu': True,
    #                      'markng@buffalo.edu': True, 'fdpilkin@buffalo.edu': True, 'msolano@buffalo.edu': True,
    #                      'ecdunbar@buffalo.edu': True, 'goktugge@buffalo.edu': True, 'eddychen@buffalo.edu': True,
    #                      'hvngo@buffalo.edu': True, 'mjkirshy@buffalo.edu': True, 'gnandi@buffalo.edu': True,
    #                      'mf89@buffalo.edu': True, 'krapivan@buffalo.edu': True, 'jwong48@buffalo.edu': True,
    #                      'jtmalina@buffalo.edu': True, 'drewwill@buffalo.edu': True, 'maxwojtc@buffalo.edu': True,
    #                      'eithneam@buffalo.edu': True, 'jerrykap@buffalo.edu': True, 'qamarraz@buffalo.edu': True,
    #                      'alin33@buffalo.edu': True, 'declansi@buffalo.edu': True, 'cesidiol@buffalo.edu': True,
    #                      'jglickma@buffalo.edu': True, 'andyqu@buffalo.edu': True, 'abassibr@buffalo.edu': True,
    #                      'johnbola@buffalo.edu': True, 'jackbett@buffalo.edu': True, 'kyotaroh@buffalo.edu': True,
    #                      'jphiggin@buffalo.edu': True, 'pdhakite@buffalo.edu': True, 'maneira@buffalo.edu': True,
    #                      'sjlefebv@buffalo.edu': True, 'jameskan@buffalo.edu': True, 'yli274@buffalo.edu': True,
    #                      'thankama@buffalo.edu': True, 'kuanlu@buffalo.edu': True, 'dengxinw@buffalo.edu': True,
    #                      'aoslica@buffalo.edu': True, 'atkamens@buffalo.edu': True, 'wupangye@buffalo.edu': True,
    #                      'jshess@buffalo.edu': True, 'brbrown2@buffalo.edu': True, 'amccarso@buffalo.edu': True,
    #                      'deniskee@buffalo.edu': True, 'djchen2@buffalo.edu': True, 'wpfarrel@buffalo.edu': True,
    #                      'tcleboff@buffalo.edu': True, 'jameshow@buffalo.edu': True, 'kwong27@buffalo.edu': True,
    #                      'junweima@buffalo.edu': True, 'jahleelp@buffalo.edu': True, 'dushanes@buffalo.edu': True,
    #                      'qiguangh@buffalo.edu': True, 'dawrotno@buffalo.edu': True, 'choongli@buffalo.edu': True,
    #                      'bvwillia@buffalo.edu': True, 'sgovil@buffalo.edu': True, 'khoryenr@buffalo.edu': True,
    #                      'hlin42@buffalo.edu': True, 'dongkyul@buffalo.edu': True, 'agolden2@buffalo.edu': True,
    #                      'kmokzhen@buffalo.edu': True, 'apeters9@buffalo.edu': True, 'baumann3@buffalo.edu': True,
    #                      'jaredbos@buffalo.edu': True, 'rlee23@buffalo.edu': True, 'sohamdes@buffalo.edu': True,
    #                      'jaredtyl@buffalo.edu': True, 'bohanghu@buffalo.edu': True, 'haoquanz@buffalo.edu': True,
    #                      'rapatel4@buffalo.edu': True, 'pdkadam@buffalo.edu': True, 'xlin44@buffalo.edu': True,
    #                      'dkim39@buffalo.edu': True, 'asharipo@buffalo.edu': True, 'samahmoo@buffalo.edu': True,
    #                      'aashokku@buffalo.edu': True, 'gmashby@buffalo.edu': True, 'cwilensk@buffalo.edu': True,
    #                      'ooladepo@buffalo.edu': True, 'jerrylay@buffalo.edu': True, 'andriyme@buffalo.edu': True,
    #                      'sarahpet@buffalo.edu': True, 'kkdas@buffalo.edu': True, 'maishasa@buffalo.edu': True,
    #                      'rlin8@buffalo.edu': True, 'mli63@buffalo.edu': True, 'jdchin@buffalo.edu': True,
    #                      'tahaalam@buffalo.edu': True, 'dchu2@buffalo.edu': True, 'mkaba2@buffalo.edu': True,
    #                      'xiongyin@buffalo.edu': True, 'chunhimw@buffalo.edu': True, 'markoka@buffalo.edu': True,
    #                      'anubhavs@buffalo.edu': True, 'jiaweisz@buffalo.edu': True, 'meszupil@buffalo.edu': True,
    #                      'jiaqiche@buffalo.edu': True, 'mdsulavk@buffalo.edu': True, 'tingpeng@buffalo.edu': True,
    #                      'elidavid@buffalo.edu': True}
    # Scroll down the page to force the row rendering.
    for i in range(0, 100):
        driver.execute_script("window.scrollTo(0,document.body.scrollHeight);")

    # Propagate the hidden table info by clicking on all email addresses.
    submissionsTableRows = driver.find_element_by_id('grades').find_elements_by_class_name('sorting_1')
    for submissionsRow in submissionsTableRows:
        submissionsRow.find_elements_by_tag_name('a')[0].click()

    # Extract the page with all submissions from a single student and collect the links.
    submissionsTableRows = driver.find_element_by_id('grades').find_elements_by_class_name('sorting_1')
    submissionsLinks = []
    studentsSubmissionInfo = {}
    for submissionsRow in submissionsTableRows:
        emailAddress = str(submissionsRow.find_element_by_class_name('email').get_attribute('innerText'))
        studentsSubmissionInfo[emailAddress] = {}
        # print('Email: "' + emailAddress + '"')
        submissionsLinks.append(submissionsRow.find_elements_by_tag_name('a')[5].get_attribute('href'))
        studentsSubmissionInfo[emailAddress] = {
            'submissionLink': str(submissionsRow.find_elements_by_tag_name('a')[5].get_attribute('href')),
            'maxScoreSubmission': 0, 'maxScoreSubmissionNumber': 1000}

    # Visit each student's submissions page.
    tweakedStudents = []
    for email, submissionInfo in studentsSubmissionInfo.items():
        if email in currentEnrollment:
            driver.get(submissionInfo['submissionLink'])

            # determine number of submissions
            studentSubmissionsTableRows = driver.find_element_by_class_name('highlight').find_element_by_tag_name('tbody').find_elements_by_tag_name('tr')
            numberOfSubmissions = len(studentSubmissionsTableRows)

            # Regrade every submission of the student.
            submissionNumber = numberOfSubmissions
            submissionInfo['maxScoreSubmissionNumber'] = numberOfSubmissions
            for studentSubmissionRow in studentSubmissionsTableRows:
                submissionScore = 0
                for score in studentSubmissionRow.find_elements_by_class_name('prettyBorder'):
                        submissionScore = submissionScore + int(float(score.get_attribute('innerText')))

                if submissionInfo['maxScoreSubmission'] < submissionScore:
                    submissionInfo['maxScoreSubmission'] = submissionScore

                # Update max score submission index
                if submissionInfo['maxScoreSubmission'] == submissionScore and submissionScore != 0:
                    submissionInfo['maxScoreSubmissionNumber'] = submissionNumber

                submissionNumber = submissionNumber - 1

            # Update top score if it is lower than best score.
            lastSubmissionScore = 0
            lastSubmissionRow = studentSubmissionsTableRows[0]
            for score in lastSubmissionRow.find_elements_by_class_name('prettyBorder'):
                lastSubmissionScore = lastSubmissionScore + int(float(score.get_attribute('innerText')))

            if lastSubmissionScore < submissionInfo['maxScoreSubmission']:
                editLinkElement = lastSubmissionRow.find_elements_by_tag_name('a')[-1]
                if str(editLinkElement.get_attribute('innerText')) == '(Edit)':
                    editLinkElement.click()

                    # Get input element for tweak value.
                    tweakAmountInputElement = driver.find_element_by_id('submission_tweak_attributes_value')
                    tweakAmountInputElement.clear()

                    # Adjust score by difference between latest submission score and the maximum score.
                    tweakAmountInputElement.send_keys(str(submissionInfo['maxScoreSubmission'] - lastSubmissionScore))

                    # Commit changes.
                    driver.find_elements_by_name('commit')[0].click()

                    # Note student that was adjusted.
                    tweakedStudents.append(email)
                else:
                    print('Error getting edit link for ' + email)

    print(tweakedStudents)