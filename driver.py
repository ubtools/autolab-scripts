from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from compute_max import collect_max_scores
# from updateMax import update_max_scores
from regrade_all import regrade_all_submissions
from computeUnusedGracedays import collect_gdu_pld
from early_bonus import early_bonus, zero_tweaks
from submission_times import aggregate_latest_submission_times, aggregate_all_submission_times
from upload_files import upload_files
import csv
from datetime import date

import sys

if len(sys.argv) < 5:
    print('usage: python3 driver.py course chromedriver roster assignments')
    print('  course: name of course on autolab. E.g., cse250-f18')
    print('  chromedriver: path to chromedriver executable')
    print('  roster: path to course roster csv file. Can be downloaded from UBLearns using comma separator.')
    print('          expected format: "Last Name","First Name","Username","Student ID","Last Access","Availability"')
    print('  assignments: comma separated list of assignments (no spaces)')
    print('          assignment string can be obtained from url for assignment in Autolab')
    print('          E.g., https://autograder.cse.buffalo.edu/courses/cse250-f18/assessments/a1programming')
    print('                has assignment string a1programming')
    sys.exit()

# sys.argv[1] -- Autolab course.
course = sys.argv[1]

# sys.argv[2] -- path to chromedriver.
pathToChromedriver = sys.argv[2]
#  Load Chromedriver in incognito mode (so nothing is saved)
options = webdriver.ChromeOptions()
options.add_argument('--incognito')
driver = webdriver.Chrome(pathToChromedriver,0,options)

# Load Autolab
driver.get('https://autograder.cse.buffalo.edu/')

# Click login
signInPanel = driver.find_element_by_class_name('sign-in-panel')
loginButton = signInPanel.find_element_by_class_name('btn')
loginButton.click()

# Wait for user to enter login credentials
#time.sleep(5)
#course = input('----\n(e.g., if the url displayed is https://autograder.cse.buffalo.edu/courses/cse111-s18/, enter cse111-s18)\nEnter Autolab course string: ')
# course = 'cseXXX-fXX'
#driver.get('https://autograder.cse.buffalo.edu/courses/'+course+'/assessments')

# Go to assignment
#assignment = str(input('----\n(e.g., if the url displayed for the assignment is https://autograder.cse.buffalo.edu/courses/CSE111-s18/assessments/quiz1, enter quiz1)\nEnter assignment string: '))

person_number_to_ubit_map = {}
ubit_map_to_person_number = {}

# print('Roster file should be the of the format of the UB Learns user information')
# print('"Last Name","First Name","Username","Student ID","Last Access","Availability"')
# print('Only the first 4 columns are necessary.')
#roster_path = input('Enter the name of the roster file (including its full path): ')


# roster_path = "roster.csv"
# sys.argv[3] -- roster path
roster_path = sys.argv[3]
roster_emails = []
with open(roster_path,newline='') as roster_file:
    reader = csv.reader(roster_file, delimiter=',', quoting=csv.QUOTE_MINIMAL)

    # Skip header row.
    next(reader,None)

    for row in reader:
        # Map person number to ubit name.
        person_number_to_ubit_map[str(row[3]).strip()] = str(row[2]).strip()

        # Map ubit name to person number.
        ubit_map_to_person_number[str(row[2]).strip()] = str(row[3]).strip()

        # Create emails from roster.
        roster_emails.append(str(row[2]).strip()+'@buffalo.edu')

print('Loaded', len(roster_emails),'students.')
# You need to setup how you map from the files to their person number OR to their ubit/email.
# PDF number to ubit name mapping
# pdf_number_to_ubit_map = {}


# assignments_string = input('Enter assignment name link(s) (comma separated): ')
# sys.argv[4] -- assignments
assignments_string = sys.argv[4]
assignments = assignments_string.split(',')

# deadline_string = input('Enter due date string with hyphens (YYYY-MM-DD-HH): ')
# deadline_split = deadline_string.split('-')
# deadline = date(year=int(deadline_split[0]),month=int(deadline_split[1]),day=int(deadline_split[2]))

# print('Deadline:',deadline,'at',str(deadline_split[3])+':00')

# sys.argv[5] -- version-threshold-enabled.
if len(sys.argv) >= 6:
    version_threshold_enabled = sys.argv[5] == 'T'
# version_threshold_enabled = input('Is there a submission limit (version threshold)? [Y/N]: ') == 'Y'
# preserve_tweak_value = input('Preserve any previously entered tweak value? [Y/N]: ') == 'Y'

# sys.argv[6] -- deadlines YYYY-MM-DD-HH comma separated list.
if len(sys.argv) >= 7:
    deadline_strings = sys.argv[6]
    deadlines = []
    deadline_hours = []
    for deadline_string in deadline_strings.split(','):
        deadline_split = deadline_string.split('-')
        deadline = date(year=int(deadline_split[0]),month=int(deadline_split[1]),day=int(deadline_split[2]))
        deadlines.append(deadline)
        deadline_hours.append(int(deadline_split[3]))

wait = WebDriverWait(driver, 180)
try:
    wait.until(EC.title_is("Autolab Home"))
except:
    print('Took too long to authenticate/login to Autolab.')
    sys.exit(1)

# zero_tweaks(driver, course, roster_emails, assignments, deadline, int(deadline_split[3]),version_threshold_enabled)
early_bonus(driver, course, roster_emails, assignments, deadlines, deadline_hours, version_threshold_enabled)
# collect_max_scores(driver, course, assignments, roster_emails, preserve_tweak_value)
collect_max_scores(driver, course, roster_emails, assignments)
# collect_gdu_pld(driver, course, roster_emails, assignments)
# aggregate_latest_submission_times(driver, course, roster_emails, assignments)

# for assignment in assignments:
#   aggregate_all_submission_times(driver, course, roster_emails, assignment)

# upload_files(driver, course, roster_emails, assignments[0])

# max_score = int(input('Enter maximum score for the assignment: '))
# delay_amount = int(input('Enter the delay amount: '))
# regrade_all_submissions(driver, course, assignments[0], roster_emails, delay_amount, max_score)
# for assignment in assignments:
#     regrade_all_submissions(driver, course, assignment, roster_emails, 0, 100)
# Close browser
driver.close()
