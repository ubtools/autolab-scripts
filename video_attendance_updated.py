import sys
import csv
import re
import json
from datetime import date

# Convert a string containing a time as MM:SS to a float.
def convert_MM_SS_to_float(s):
    return int(s[:s.find(':')]) + int(s[s.find(':') + 1:]) / 60
# End def convert_MM_SS_to_float

if len(sys.argv) < 4:
    print(
        'usage: python3 /full/path/to/json /full/path/to/ublearns-roster-csv /full/path/to/panopto-data /full/path/to/output/ viewing-percentage')
    sys.exit()

# sys.argv[1] -- Lecture Video Playlist json
lecture_playlist_json_file = sys.argv[1]

# sys.argv[2] -- course roster csv.
roster_csv_path = sys.argv[2]

# sys.argv[3] -- Panopto Usage csv full path.
panopto_csv_path = sys.argv[3]

# sys.argv[4] -- output directory full path.
output_csv_path = sys.argv[4]

# sys.argv[5] -- viewing time padding.
if len(sys.argv) > 5:
    viewing_time_padding = float(sys.argv[5])
else:
    viewing_time_padding = 0.9

# sys.argv[6] -- start of semester -- ignore.
if len(sys.argv) > 6:
    ignore_date_split = sys.argv[6].split('-')
    ignore_before_date = date(year=int(ignore_date_split[0]), month=int(ignore_date_split[1]),
         day=int(ignore_date_split[2]))
else:
    ignore_before_date = 0

# sys.argv[6] -- number of missed videos.
if len(sys.argv) > 7:
    missed_videos_allowed = int(sys.argv[7])
else:
    missed_videos_allowed = 0

# sys.argv[7] -- excused dates as a comma separated string.
if len(sys.argv) > 8:
    excused_dates = set(sys.argv[8].split(','))
else:
    excused_dates = set()

# Determine update date for filename.
output_file_date = panopto_csv_path[panopto_csv_path.rfind('2020'):panopto_csv_path.rfind('.')]
output_file_course = panopto_csv_path[panopto_csv_path.rfind('cse'):panopto_csv_path.rfind('cse')+6]

# Import student enrollment roster.
person_number_to_ubit_map = {}
ubit_map_to_person_number = {}
roster_ubits = set()
roster_emails = set()
completion_map = {}
with open(roster_csv_path, newline='') as roster_file:
    reader = csv.reader(roster_file, delimiter=',', quoting=csv.QUOTE_MINIMAL)

    # Skip header row.
    next(reader, None)

    for row in reader:
        # Map person number to ubit name.
        person_number_to_ubit_map[str(row[3]).strip()] = str(row[2]).strip()

        # Map ubit name to person number.
        ubit_map_to_person_number[str(row[2]).strip()] = str(row[3]).strip()

        # Create emails from roster.
        student_ubit = str(row[2]).strip()
        roster_ubits.add(student_ubit)
        student_email = student_ubit + '@buffalo.edu'
        roster_emails.add(student_email)

        # Add entry to completion map.
        completion_map[student_email] = {'LectureVideos': {}, 'Attendance' : {}, 'Feedback' : {}}
    # End for
# End with
del roster_csv_path
del roster_file

# Import JSON data for video playlist.
with open(lecture_playlist_json_file) as playlist_json:
    video_playlist_json = json.load(playlist_json)
del playlist_json

# Process JSON into usable format.
lecture_date_order = sorted(list(map(lambda s: s[s.find('2020'):s.rfind(' ')],video_playlist_json.keys())))
attendance_video_title_set = {video_title for playlist in video_playlist_json.values() for video_title in playlist}
video_title_duration_map = {video_title : convert_MM_SS_to_float(duration_string) for playlist in video_playlist_json.values() for (video_title,duration_string) in playlist.items()}
video_title_deadline_map = {video_title : playlist_title[playlist_title.find('2020'):playlist_title.rfind(' ')] for (playlist_title,playlist) in video_playlist_json.items() for video_title in playlist}
lecture_date_total_duration_map = {playlist_title[playlist_title.find('2020'):playlist_title.rfind(' ')] : int(sum(map(convert_MM_SS_to_float,playlist.values()))) for (playlist_title,playlist) in video_playlist_json.items() }


# Common utility regex for testing/parsing rows.
date_pattern = re.compile('2020-[01][0-9]-[0-3][0-9]')
email_pattern = re.compile('[a-zA-Z]+[0-9]*@buffalo\.edu')
# Panopto row format:
# Timestamp,Folder Name,Folder ID,Session Name,Session ID,Minutes Delivered,UserName,User ID,Name,Email,Viewing Type,Root Folder (Level 0)
date_map_to_session_names = {}
with open(panopto_csv_path, newline='') as panopto_file:
    reader = csv.reader(panopto_file, delimiter=',', quoting=csv.QUOTE_MINIMAL)

    # Skip header row.
    next(reader, None)

    for row in reader:
        student_email = row[9].lower()
        if not email_pattern.search(student_email):
            student_email = row[6].lower()
            student_email = student_email[student_email.find('\\')+1:] + "@buffalo.edu"
        # End if

        if not email_pattern.search(student_email):
            # Unexpected entry without UBIT or UB email address.
            print(row)
            continue
            raise Exception()
        # End if

        if student_email and student_email in roster_emails:
            session_name = row[3]
            # Skip videos that do not count for attendance.
            if session_name not in attendance_video_title_set:
                continue

            view_timestamp_split = row[0][:row[0].find(' ')].split('/')
            view_timestamp_date = date(year=int(view_timestamp_split[2]), month=int(view_timestamp_split[0]),
                                       day=int(view_timestamp_split[1]))

            deadline_date_string = video_title_deadline_map[session_name]
            video_length = video_title_duration_map[session_name]
            date_split = deadline_date_string.split('-')
            deadline_date = date(year=int(date_split[0]), month=int(date_split[1]), day=int(date_split[2]))

            # Check if session was viewed after the deadline.
            if (view_timestamp_date - deadline_date).days >= 1:
                continue

            # Add entry to completion map if it doesn't exist yet for that date.
            if deadline_date_string not in completion_map[student_email]['LectureVideos']:
                completion_map[student_email]['LectureVideos'][deadline_date_string] = {}

            # Add duration of viewing time to the respective video.
            if session_name in completion_map[student_email]['LectureVideos'][deadline_date_string]:
                completion_duration = completion_map[student_email]['LectureVideos'][deadline_date_string][session_name]
                completion_duration += float(row[5])
                completion_map[student_email]['LectureVideos'][deadline_date_string][session_name] = min(completion_duration,video_length)
            else:
                completion_map[student_email]['LectureVideos'][deadline_date_string][session_name] = min(float(row[5]),video_length)

            if deadline_date_string not in date_map_to_session_names:
                date_map_to_session_names[deadline_date_string] = set()

            date_map_to_session_names[deadline_date_string].add(session_name)
    # End for
# End with
del row
del completion_duration
del video_length
del panopto_csv_path
del panopto_file
del view_timestamp_date
del view_timestamp_split
del deadline_date
del deadline_date_string
del date_split
del session_name
del student_ubit
del date_pattern

# Process student attendance per lecture and compute attendance feedback.
roster_attendance = {}
for student_email in roster_emails:
    student_attendance_map = {}
    roster_attendance[student_email] = student_attendance_map
    student_completion_entry = completion_map[student_email]['LectureVideos']
    for lecture_date in lecture_date_total_duration_map:
        if lecture_date in student_completion_entry:
            total_view_time = sum(student_completion_entry[lecture_date].values())
        else:
            total_view_time = 0


        marked_present = (total_view_time >= lecture_date_total_duration_map[lecture_date] * viewing_time_padding)
        if marked_present:
            student_feedback = 'Marked present'
        else:
            if lecture_date not in student_completion_entry:
                student_feedback = 'You missed watching all lecture videos for ' + lecture_date
            else:
                student_feedback = 'You only viewed ' + str(round(total_view_time,2)) + ' out of ' + str(round(lecture_date_total_duration_map[lecture_date],2)) + ' minutes of lecture for ' + lecture_date + '.\\n'
                for video_title in sorted(date_map_to_session_names[lecture_date]):
                    if video_title in student_completion_entry[lecture_date]:
                        viewing_time = student_completion_entry[lecture_date][video_title]
                        video_duration = video_title_duration_map[video_title]
                        if viewing_time >= video_duration * viewing_time_padding:
                            student_feedback += video_title + ' was watched in full.\\n'
                        else:
                            student_feedback += video_title + ' was only viewed for ' + str(round(viewing_time,2)) + ' out of ' + str(round(video_duration,2)) + ' minutes.\\n'
                    else:
                        student_feedback += video_title + ' was missed entirely.\\n'

        lecture_date_split = lecture_date.split('-')
        lecture_date_object = date(year=int(lecture_date_split[0]), month=int(lecture_date_split[1]),day=int(lecture_date_split[2]))
        student_attendance_map[lecture_date] = {'total': total_view_time, 'present': (marked_present and lecture_date_object > ignore_before_date), 'feedback':student_feedback}



# Write csv file containing attendance upload for autolab.

with open(output_csv_path+output_file_course+'-attendance-scores-' + output_file_date + '.csv','w', newline='', encoding='ascii') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',')
    for (student_email, attendance_info) in roster_attendance.items():
        student_row = [student_email]
        for lecture_date in lecture_date_order:
            student_row.append(1 if roster_attendance[student_email][lecture_date]['present'] else 0)
        spamwriter.writerow(student_row)

# Write csv file containing feedback upload for autolab.
with open(output_csv_path+output_file_course+'-attendance-feedback-' + output_file_date + '.csv','w', newline='', encoding='ascii') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',')
    for (student_email, attendance_info) in roster_attendance.items():
        student_row = [student_email]
        for lecture_date in lecture_date_order:
            student_row.append(roster_attendance[student_email][lecture_date]['feedback'])
        spamwriter.writerow(student_row)

