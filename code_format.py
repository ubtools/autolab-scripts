def compute_max(seq):
	print(seq)
	if len(seq) == 1: return seq[0]
	m = compute_max(seq[:-1])
	last = seq[-1]
	if last > m: return last
	else: m

def fibonacci(n):
	if (n <= 1): return n
	else: return fibonacci(n-1) + fibonacci(n-2)