import sys
import csv
import re
from datetime import date

if len(sys.argv) != 10:
    print(
        'usage: python3 video_attendance.py lecture-prefix live-prefix panopto-full-path tophaat-full-path roster output-full-path')
    sys.exit()

# sys.argv[1] -- Lecture Video Prefix.
lecture_prefix = sys.argv[1]

# sys.argv[2] -- Live Video Prefix
live_prefix = sys.argv[2]

# sys.argv[3] -- Panopto Usage csv full path.
panopto_csv_path = sys.argv[3]

# sys.argv[4] -- TopHat gradebook csv full path.
tophat_csv_path = sys.argv[4]

# sys.argv[5] -- course roster csv.
roster_csv_path = sys.argv[5]

# sys.argv[6] -- course roster csv.
output_csv_path = sys.argv[6]

# sys.argv[7] -- minimum viewing time.
min_time = float(sys.argv[7])

# sys.argv[8] -- number of missed videos.
missed_videos_allowed = int(sys.argv[8])

# sys.argv[9] -- excused dates as a comma separated string.
excused_dates = set(sys.argv[9].split(','))

person_number_to_ubit_map = {}
ubit_map_to_person_number = {}
roster_emails = set()
completion_map = {}
with open(roster_csv_path, newline='') as roster_file:
    reader = csv.reader(roster_file, delimiter=',', quoting=csv.QUOTE_MINIMAL)

    # Skip header row.
    next(reader, None)

    for row in reader:
        # Map person number to ubit name.
        person_number_to_ubit_map[str(row[3]).strip()] = str(row[2]).strip()

        # Map ubit name to person number.
        ubit_map_to_person_number[str(row[2]).strip()] = str(row[3]).strip()

        # Create emails from roster.
        student_email = str(row[2]).strip() + '@buffalo.edu'
        roster_emails.add(student_email)

        # Add entry to completion map.
        completion_map[student_email] = {'TopHat': {}, 'LectureVideos': {}, 'LiveVideos': {}, 'Deleted': set(), 'Attendance' : {}, 'Feedback' : {}}
    # End for
# End with
del roster_csv_path
del roster_file

date_pattern = re.compile('2020-[01][0-9]-[0-3][0-9]')
part_pattern = re.compile('Pt [0-1][0-9]')
email_pattern = re.compile('[a-zA-Z]+[0-9]*@buffalo\.edu')
# Panopto row format:
# Timestamp,Folder Name,Folder ID,Session Name,Session ID,Minutes Delivered,UserName,User ID,Name,Email,Viewing Type,Root Folder (Level 0)
date_map_to_session_names = {}
with open(panopto_csv_path, newline='') as panopto_file:
    reader = csv.reader(panopto_file, delimiter=',', quoting=csv.QUOTE_MINIMAL)

    # Skip header row.
    next(reader, None)

    for row in reader:
        student_email = row[9].lower()
        if not email_pattern.search(student_email):
            student_email = row[6].lower()
            student_email = student_email[student_email.find('\\')+1:] + "@buffalo.edu"
        if not email_pattern.search(student_email):
            raise Exception()
        if student_email and student_email in roster_emails:
            session_name = row[3]
            date_match = date_pattern.search(session_name)
            if date_match:
                date_string = date_match.group(0)
                date_split = date_string.split('-')
                session_date = date(year=int(date_split[0]), month=int(date_split[1]), day=int(date_split[2]))

                view_date_split = row[0][:row[0].find(' ')].split('/')
                view_date = date(year=int(view_date_split[2]), month=int(view_date_split[0]),
                                 day=int(view_date_split[1]))

                if (view_date - session_date).days > 1:
                    continue

                if lecture_prefix in session_name:
                    if date_string not in completion_map[student_email]['LectureVideos']:
                        completion_map[student_email]['LectureVideos'][date_string] = {}

                    if session_name in completion_map[student_email]['LectureVideos'][date_string]:
                        completion_map[student_email]['LectureVideos'][date_string][session_name] += float(row[5])
                    else:
                        completion_map[student_email]['LectureVideos'][date_string][session_name] = float(row[5])

                    if date_string not in date_map_to_session_names:
                        date_map_to_session_names[date_string] = set()

                    date_map_to_session_names[date_string].add(session_name)
                elif live_prefix in session_name:
                    if session_name in completion_map[student_email]['LiveVideos']:
                        completion_map[student_email]['LiveVideos'][session_name] += float(row[5])
                    else:
                        completion_map[student_email]['LiveVideos'][session_name] = float(row[5])
    # End for
# End with
del row
del panopto_csv_path
del panopto_file
del lecture_prefix
del date_match

# Remove videos that haven't been viewed for more than a minute.
for student_email in completion_map:
    student_data = completion_map[student_email]
    field = 'LectureVideos'
    sessions_to_remove = []
    for date_string, session_name_map_to_time in student_data[field].items():
        for session_name, time in session_name_map_to_time.items():
            if time < 1:
                sessions_to_remove.append((date_string, session_name))
    for date_string,session_name in sessions_to_remove:
        del student_data[field][date_string][session_name]
        if not student_data[field][date_string]:
            del student_data[field][date_string]
        student_data['Deleted'].add((date_string,session_name))
    field = 'LiveVideos'
    sessions_to_remove = []
    for session_name, time in student_data[field].items():
        if time < min_time:
            date_string = date_pattern.search(session_name).group(0)
            sessions_to_remove.append((date_string,session_name))
    for date_string, session_name in sessions_to_remove:
        del student_data[field][session_name]
        student_data['Deleted'].add((date_string, session_name))

# print([(email,student_data['Deleted']) for email, student_data in completion_map.items()])
# Process TopHat attendance
attendance_dates = []
students_on_tophat = set()
with open(tophat_csv_path, newline='') as tophat_file:
    reader = csv.reader(tophat_file, delimiter=',', quoting=csv.QUOTE_MINIMAL)

    # Process first header row to find attendance columns range.
    header = next(reader, None)
    attend_col_start = -1
    attend_col_end = -1
    for i, entry in enumerate(header):
        if entry == 'Attendance':
            attend_col_start = i
        elif attend_col_start != -1 and entry != '':
            # At first TopHat questions folder.
            # Upperbound for attendance is 1 column back.
            attend_col_end = i - 1
            break

    # Process second header row to find attendance dates.
    header = next(reader, None)
    for i, entry in enumerate(header[attend_col_start:attend_col_end]):
        if i % 2 == 0:
            date_string = entry[:entry.find('T')]
            attendance_dates.append(date_string)
        else:
            continue

    for row in reader:
        student_email = row[4].lower()
        if student_email in roster_emails:
            students_on_tophat.add(student_email)
            for i, entry in enumerate(row[attend_col_start:attend_col_end]):
                if i % 2 == 0:
                    completion_map[student_email]['TopHat'][attendance_dates[int(i / 2)]] = entry
                else:
                    continue
    # End for
# End with

for student_email in roster_emails:
    if student_email not in students_on_tophat:
        for date_string in attendance_dates:
            completion_map[student_email]['TopHat'][date_string] = '0'

# Create feedback for attendance.
for student_email in completion_map:
    student_data = completion_map[student_email]
    for date_string in attendance_dates:
        if student_data['TopHat'][date_string] == 'excused':
            student_data['Attendance'][date_string] = 1
            student_data['Feedback'][date_string] = 'Absence was excused. You will be marked as 1 in the gradebook but will not be penalized for TopHat.'
        else:
            topHatAttended = student_data['TopHat'][date_string] == '1'
            liveVideoWatched = live_prefix + date_string in student_data['LiveVideos']
            if date_string in date_map_to_session_names:
                lectureVideosViewed = date_string in student_data['LectureVideos'] and len(student_data['LectureVideos'][date_string]) >= (len(date_map_to_session_names[date_string]) - missed_videos_allowed)
                if (topHatAttended or liveVideoWatched) and lectureVideosViewed:
                    student_data['Attendance'][date_string] = 1
                    student_data['Feedback'][date_string] = 'Marked present for ' + date_string + '.'
                else:
                    student_data['Attendance'][date_string] = 0
                    if not (topHatAttended or liveVideoWatched):
                        student_data['Feedback'][date_string] = "Missed live lecture and didn't watch recording."
                    if not lectureVideosViewed:
                        if date_string in student_data['Feedback']:
                            if date_string not in student_data['LectureVideos']:
                                student_data['Feedback'][
                                    date_string] += " Also missed all prerecorded lecture videos on " + date_string + '.'
                            else:
                                lectures_viewed = len(student_data['LectureVideos'][date_string])
                                lectures_required = len(date_map_to_session_names[date_string])
                                lectures_missed = lectures_required - lectures_viewed
                                student_data['Feedback'][date_string] += " Also missed " + str(lectures_missed) + '/' + str(lectures_required) + " prerecorded lecture video(s): "
                                for required_session in sorted(date_map_to_session_names[date_string]):
                                    if required_session not in student_data['LectureVideos'][date_string]:
                                        student_data['Feedback'][date_string] += required_session
                                        student_data['Feedback'][date_string] += ', '
                                student_data['Feedback'][date_string] = student_data['Feedback'][date_string][:-2] + '.'
                        else:
                            if date_string not in student_data['LectureVideos']:
                                student_data['Feedback'][
                                    date_string] = "Missed all prerecorded lecture videos on " + date_string + '.'
                            else:
                                lectures_viewed = len(student_data['LectureVideos'][date_string])
                                lectures_required = len(date_map_to_session_names[date_string])
                                lectures_missed = lectures_required - lectures_viewed
                                student_data['Feedback'][date_string] = "Missed " + str(lectures_missed) + '/' + str(lectures_required) + " prerecorded lecture video(s): "
                                for required_session in sorted(date_map_to_session_names[date_string]):
                                    if required_session not in student_data['LectureVideos'][date_string]:
                                        student_data['Feedback'][date_string] += required_session
                                        student_data['Feedback'][date_string] += ', '
                                student_data['Feedback'][date_string] = student_data['Feedback'][date_string][:-2] + '.'
            else:
                month = int(date_string[5:7])
                day = int(date_string[-2:])
                if topHatAttended:
                    student_data['Attendance'][date_string] = 1
                    student_data['Feedback'][date_string] = "Marked present on TopHat for " + date_string

                    if month < 3 or (month == 3 and day < 23):
                        student_data['Feedback'][date_string] += " (before online videos)."
                    else:
                        student_data['Feedback'][date_string] += '.'
                elif liveVideoWatched:
                    student_data['Attendance'][date_string] = 1
                    student_data['Feedback'][date_string] = "Marked present for " + date_string + " for watching lecture video."
                else:
                    student_data['Attendance'][date_string] = 0
                    student_data['Feedback'][date_string] = "Marked absent on TopHat for " + date_string
                    if month < 3 or (month == 3 and day < 23):
                        student_data['Feedback'][date_string] += " (before online videos)."
                    else:
                        student_data['Feedback'][date_string] += ' and missed video lecture.'
            if date_string in excused_dates:
                student_data['Attendance'][date_string] = 1


max_len = {'TopHat': max(len(completion_map[k]['TopHat']) for k in completion_map),
           'LectureVideos': max(len(completion_map[k]['LectureVideos']) for k in completion_map),
           'LiveVideos': max(len(completion_map[k]['LiveVideos']) for k in completion_map),
           'Deleted': max(len(completion_map[k]['Deleted']) for k in completion_map)}

# Write csv file containing modified students and also double charged students.
with open(output_csv_path, 'w', newline='', encoding='ascii') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',')

    row_entry_data = ['Email']
    for field in ['Attendance','TopHat', 'LectureVideos', 'LiveVideos', 'Deleted']:
        if field == 'Attendance':
            for date_string in reversed(attendance_dates):
                row_entry_data.append(date_string)
                row_entry_data.append(date_string+'_feedback')
        elif field == 'TopHat':
            for date_string in reversed(attendance_dates):
                row_entry_data.append(date_string)
        else:
            row_entry_data.append(field)
            for i in range(0, max_len[field] - 1):
                row_entry_data.append('')
        row_entry_data.append('xx')
    # Write header row.
    spamwriter.writerow(row_entry_data)

    # Write student data rows.
    for student_email in roster_emails:
        student_data = completion_map[student_email]
        row_entry_data = [student_email]
        for field in ['Attendance', 'TopHat', 'LectureVideos', 'LiveVideos', 'Deleted']:
            if field == 'Attendance':
                for date_string in sorted(attendance_dates):
                    row_entry_data.append(student_data['Attendance'][date_string])
                    row_entry_data.append(student_data['Feedback'][date_string])
            else:
                for entry in sorted(student_data[field]):
                    if field == 'TopHat':
                        row_entry_data.append(student_data[field][entry])
                    else:
                        row_entry_data.append(entry)
                for i in range(0, max_len[field] - len(student_data[field])):
                    row_entry_data.append('')
            row_entry_data.append('xx')
        spamwriter.writerow(row_entry_data)
