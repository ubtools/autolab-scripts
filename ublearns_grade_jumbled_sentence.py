from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import sys
import traceback

def scroll_into_view_then_click(driver,element):
    driver.execute_script("arguments[0].scrollIntoView(true);", element)
    driver.execute_script("arguments[0].click();", element)

def open_and_check_rubrics(driver, main_window, delay_amount):
    # Collect view rubric buttons.
    view_rubric_buttons = driver.find_elements(By.CSS_SELECTOR, 'span.stepTitleRight.liveArea > p.taskbuttondiv > input.genericButton')
    missing_list = []
    has_all_selections = True
    for rubric_button in view_rubric_buttons:
        # Open student feedback.
        scroll_into_view_then_click(driver, rubric_button)
        time.sleep(delay_amount)
        # Move the driver to new window.
        rubric_window = driver.window_handles[1]
        driver.switch_to.window(rubric_window)
        # Extract grader feedback.
        selected_cell_count = len(driver.find_elements(By.CSS_SELECTOR, 'tr.rubricGradingRow > td.rubricGradingCell.selectedCell'))
        table_header_list = driver.find_elements(By.CSS_SELECTOR, 'tr.rubricGradingRow > th')
        has_all_selections = has_all_selections and table_header_list and (selected_cell_count == len(table_header_list))
        if not has_all_selections:
            table_rows_list = driver.find_elements(By.CSS_SELECTOR, 'tr.rubricGradingRow')
            for i,row in enumerate(table_rows_list):
                if len(row.find_elements(By.CSS_SELECTOR,'td.rubricGradingCell.selectedCell')) == 0:
                    missing_list.append(table_header_list[i].text)

        # Exit rubric page.
        exit_button = driver.find_element(By.CSS_SELECTOR, 'div.taskbuttondiv_wrapper > p.taskbuttondiv[id="bottom_submitButtonRow"] > input[name="bottom_Exit"]')
        scroll_into_view_then_click(driver, exit_button)
        # Switch back to main window.
        driver.switch_to.window(main_window)
    # Return whether or not the rubric is fully completed.
    return (has_all_selections,missing_list)

if len(sys.argv) != 3:
    print('usage: python3 driver.py chromedriver grading_url')
    print('  chromedriver: path to chromedriver executable')
    print('  grading_url: UB Learns URL to Course Tools > Grade Center > ')
    sys.exit()


# sys.argv[1] -- path to chromedriver.
pathToChromedriver = sys.argv[1]
#  Load Chromedriver in incognito mode (so nothing is saved)
options = webdriver.ChromeOptions()
options.add_argument('--incognito')
driver = webdriver.Chrome(pathToChromedriver,0,options)

delay_amount = 1
# Load Autolab
driver.get('https://ublearns.buffalo.edu/')

# Click login
loginButton = driver.find_element_by_class_name('login')
loginButton.click()

# input('After logging into UB Learns and Loading Grading Page on Chrome webdriver, press enter...')

wait = WebDriverWait(driver, 180)
try:
    wait.until(EC.title_is("Help and Updates"))
except:
    print('Took too long to authenticate/login to UB Learns.')
    sys.exit(1)

# Collect rubrics from current problem.
main_window = driver.current_window_handle
# Use switch_to.window(handle) to switch windows.
# driver.switch_to.window(main_window);

msg = ''

# sys.argv[2] -- path to chromedriver.
grading_url = sys.argv[2]
driver.get(grading_url)

input('Open "Grade Questions > Select Jumbled Question Problem" for the exam to check, navigate to the first student, and then press enter...')
previous_index = ''
previous_name = ''
current_index = ''
final_index = ''
start_index = 0
with open('cse250-f20-jumbled-log.txt','a') as log_file:
    while msg.lower() != 'n':
        try:
            problems_to_regrade = len(driver.find_elements(By.CSS_SELECTOR,'td > span.table-data-cell-value > a[href*="javascript"]'))
            for current_problem_index in range(start_index,problems_to_regrade):
                grading_link_element_list = driver.find_elements(By.CSS_SELECTOR,'td > span.table-data-cell-value > a[href*="javascript"]')
                next_problem_link = grading_link_element_list[current_problem_index]
                scroll_into_view_then_click(driver,next_problem_link)
                time.sleep(delay_amount)
                # Check that answer text expanded.
                question_expanded = len(driver.find_elements(By.CSS_SELECTOR,'div.infoListWrapper > h3[id] > a.open')) == 1
                if not question_expanded:
                    expand_link = driver.find_elements(By.CSS_SELECTOR,'div.infoListWrapper > h3[id] > a')
                    if len(expand_link) != 1: raise Exception('Mismatch: found multiple expand links.')
                    expand_link[0].click()
                    time.sleep(0.25)
                    question_expanded = len(driver.find_elements(By.CSS_SELECTOR, 'div.infoListWrapper > h3[id] > a.open')) == 1
                    if not question_expanded:
                        raise Exception('Error: could not expand question text.')

                # Collect correct answers.
                answers_list = list(map(lambda e: e.text.strip(),driver.find_elements(By.CSS_SELECTOR,'td.valueCell > div.vtbegenerated.inlineVtbegenerated > ol> li > strong')))
                print(f'Loaded answers: {answers_list}')

                # Check student answers.
                scores_list = list(map(lambda e: int(e.text[:e.text.find('.')].strip()),driver.find_elements(By.CSS_SELECTOR, 'strong.scr')))
                student_answer_lists = list(map(lambda e: list(map(lambda t: t.strip(),e.text.split(','))),driver.find_elements(By.CSS_SELECTOR,'div.details > table.key-valueTable > tbody > tr > td[id="meta_value_2"]')))
                number_of_students = len(scores_list)
                if len(scores_list) != len(student_answer_lists): raise Exception('Mismatch: student scores vs student answers.',scores_list,student_answer_lists)
                if len(scores_list) != number_of_students: raise Exception('Mismatch: student scores vs student answers vs number of students.', scores_list, student_answer_lists)
                changed_student_indices = []
                for student_index,(student_calculated_score,student_answers) in enumerate(zip(scores_list,student_answer_lists)):
                    points_earned = 0
                    if len(answers_list) != len(student_answers): raise Exception('Mismatch: student answers vs expected no. answers.', answers_list, student_answer_lists)
                    for ans_index in range(len(answers_list)):
                        if student_answers[ans_index] == answers_list[ans_index]:
                            points_earned += 2
                    if points_earned != student_calculated_score:
                        changed_student_indices.append((student_index,points_earned))

                student_name_list = list(map(lambda e: e.text[:e.text.find('(')].strip(),driver.find_elements(By.CSS_SELECTOR,'ul.contentListPlain.gradingList > li.clearfix.liItem > div.item.clearfix > h3[id]')))
                if changed_student_indices:
                    for changed_index,points_earned in changed_student_indices:
                        log_file.write(f'Updating {student_name_list[changed_index]} from {scores_list[changed_index]} to {points_earned}.\n')
                        print(f'Updating {student_name_list[changed_index]} from {scores_list[changed_index]} to {points_earned}.')
                        edit_buttons_list = driver.find_elements(By.CSS_SELECTOR,'a.button-5.edtbtn')
                        if len(edit_buttons_list) != number_of_students: raise Exception('Mismatch: edit button vs number of students.', edit_buttons_list, number_of_students)
                        edit_button = edit_buttons_list[changed_index]
                        scroll_into_view_then_click(driver,edit_button)
                        time.sleep(0.2)
                        score_fields = driver.find_elements(By.CSS_SELECTOR,'input#scoreField')
                        if len(score_fields) != 1: raise Exception('Error: Found multiple score field elements.')
                        score_fields[0].clear()
                        score_fields[0].send_keys(str(points_earned))
                        time.sleep(0.1)
                        submit_buttons = driver.find_elements(By.CSS_SELECTOR,'input#submitButton')
                        if len(submit_buttons) != 1: raise Exception('Error: Found multiple submit button elements.')
                        submit_buttons[0].click()
                        time.sleep(0.2)
                # end if changed_student_indices
                # go back to main page
                back_button = driver.find_element(By.CSS_SELECTOR,'div.u_reverseAlign.u_controlsWrapper > a')
                scroll_into_view_then_click(driver,back_button)
                time.sleep(2*delay_amount)
            # End for loop looking at all problems.
            # Finished regrading, end while.
            msg = 'n'
        except Exception as err:
            # exc_type, exc_value, exc_traceback = sys.exc_info()
            # print('Driver window title',driver.title)
            # print("*** print_tb:")
            # traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
            # print("*** print_exception:")
            # # exc_type below is ignored on 3.5 and later
            # traceback.print_exception(exc_type, exc_value, exc_traceback,
            #                           limit=2, file=sys.stdout)
            # print("*** print_exc:")
            # traceback.print_exc(limit=2, file=sys.stdout)
            # print("*** format_exc, first and last line:")
            # formatted_lines = traceback.format_exc().splitlines()
            # print(formatted_lines[0])
            # print(formatted_lines[-1])
            # print("*** format_exception:")
            # # exc_type below is ignored on 3.5 and later
            # print(repr(traceback.format_exception(exc_type, exc_value,
            #                                       exc_traceback)))
            # print("*** extract_tb:")
            # print(repr(traceback.extract_tb(exc_traceback)))
            # print("*** format_tb:")
            # print(repr(traceback.format_tb(exc_traceback)))
            # print("*** tb_lineno:", exc_traceback.tb_lineno)
            # time.sleep(delay_amount)
            print(err)
            msg = input(f'UB Learns kicked back to gradebook (most likely). Is it fixed?\nLast index: {previous_index}. Last student processed:{previous_name}.\nPress enter to continue when ready to proceed, n to quit...')

        # msg = input('Continue? (Y/N): ')


# Close browser
driver.close()
