from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import sys
import traceback
import csv

def scroll_into_view_then_click(driver,element):
    driver.execute_script("arguments[0].scrollIntoView(true);", element)
    driver.execute_script("arguments[0].click();", element)

def check_selected_rubric_cells(driver, table_header_list, missing_list, student_output_row):
    selected_cell_count = len(driver.find_elements(By.CSS_SELECTOR, 'tr.rubricGradingRow > td.rubricGradingCell.selectedCell'))

    has_all_selections = table_header_list and (selected_cell_count == len(table_header_list))
    if not has_all_selections:
        table_rows_list = driver.find_elements(By.CSS_SELECTOR, 'tr.rubricGradingRow')
        for i, row in enumerate(table_rows_list):
            if len(row.find_elements(By.CSS_SELECTOR, 'td.rubricGradingCell.selectedCell')) == 0:
                missing_list.append(table_header_list[i].text)
    # Extract rubric values.
    table_rows_list = driver.find_elements(By.CSS_SELECTOR, 'tr.rubricGradingRow')
    rubric_column_titles = list(map(lambda e: e.text.strip(), driver.find_elements(By.CSS_SELECTOR, 'a.button-4')))
    for i, row in enumerate(table_rows_list):
        selected_cell = row.find_elements(By.CSS_SELECTOR, 'td.rubricGradingCell.selectedCell')
        if len(selected_cell) > 1: raise Exception(f'Error: expected exactly 1 or 0 selected cells, but found {len(selected_cell)} selected cells instead.')
        if len(selected_cell) == 0:
            student_output_row.append('XX')
        else:
            selected_cell_id = selected_cell[0].get_attribute('rubriccellid')
            rubric_cell_id_list = list(map(lambda e: e.get_attribute('rubriccellid'),row.find_elements(By.CSS_SELECTOR, 'td.rubricGradingCell')))
            selected_index = rubric_cell_id_list.index(selected_cell_id)
            student_output_row.append(rubric_column_titles[selected_index])
    return has_all_selections

def extract_rubric_grade(driver):
    rubric_grade_label_text = driver.find_element(By.CSS_SELECTOR, 'span.rubricGradingCalcTotalLabel').text.strip()
    rubric_calculated_points = float(
        rubric_grade_label_text[rubric_grade_label_text.find(':') + 1:rubric_grade_label_text.find('(')].strip())
    return rubric_calculated_points

def open_and_swtich_to_rubric(driver, rubric_button):
    # Open rubric page.
    scroll_into_view_then_click(driver, rubric_button)
    # Wait for rubric window to appear.
    WebDriverWait(driver, 10).until(EC.number_of_windows_to_be(2))
    # Move the driver to new window.
    rubric_window = driver.window_handles[1]
    driver.switch_to.window(rubric_window)
    # Wait for window to load.
    WebDriverWait(driver, 10).until(EC.title_contains('Rubric Detail'))

def open_and_check_rubrics(driver, main_window, delay_amount, student_output_row):
    # Collect view rubric buttons.
    view_rubric_buttons = driver.find_elements(By.CSS_SELECTOR, 'span.stepTitleRight.liveArea > p.taskbuttondiv > input.genericButton')
    missing_list = []
    has_all_selections = True
    current_scores_on_exam = list(map(lambda e: float(e.get_attribute('value').strip()),driver.find_elements(By.XPATH, '//p[@class="taskbuttondiv"]/input[@class="genericButton"]/../label/input[@type="text"]')))
    for index, rubric_button in enumerate(view_rubric_buttons):
        # Collect current score for problem.
        current_exam_points = current_scores_on_exam[index]
        # Open and switch to rubric.
        open_and_swtich_to_rubric(driver, rubric_button)
        # Extract grader feedback.
        table_header_list = driver.find_elements(By.CSS_SELECTOR, 'tr.rubricGradingRow > th')
        if len(table_header_list) == 0:
            # Multiple rubrics used.
            number_of_rubrics = len(driver.find_elements(By.CSS_SELECTOR, 'h4.u_controlsWrapper'))
            if number_of_rubrics <= 1: raise Exception(f'Error: expected multiple rubrics, but found {number_of_rubrics} rubrics instead.')
            for rubric_index in range(number_of_rubrics):
                rubric_link_header = driver.find_elements(By.CSS_SELECTOR, 'h4.u_controlsWrapper')[rubric_index]
                is_grading_rubric = len(rubric_link_header.find_elements(By.CSS_SELECTOR, 'img[alt="Used for Grading"]')) == 1
                # Load rubric page.
                rubric_link = rubric_link_header.find_element(By.CSS_SELECTOR, 'a')
                rubric_link.click()
                # Wait for rubric next page to load.
                WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR,'tr.rubricGradingRow > th')))
                # Grab list of table headers.
                table_header_list = driver.find_elements(By.CSS_SELECTOR, 'tr.rubricGradingRow > th')
                has_all_selections = check_selected_rubric_cells(driver,table_header_list,missing_list,student_output_row) and has_all_selections
                if is_grading_rubric:
                    rubric_calculated_points = extract_rubric_grade(driver)
                    student_output_row.append(str(current_exam_points))
                    student_output_row.append(str(rubric_calculated_points))

                # Exit rubric page.
                exit_button = driver.find_element(By.CSS_SELECTOR,
                                                  'div.taskbuttondiv_wrapper > p.taskbuttondiv[id="bottom_submitButtonRow"] > input[name="bottom_Exit"]')
                scroll_into_view_then_click(driver, exit_button)
                # Switch back to main window.
                driver.switch_to.window(main_window)
                # Open and switch to rubric.
                open_and_swtich_to_rubric(driver, rubric_button)
            # Exit rubric page.
            exit_button = driver.find_element(By.CSS_SELECTOR,'p.backLink > a')
            scroll_into_view_then_click(driver, exit_button)
        else:
            # Single rubric used.
            has_all_selections = check_selected_rubric_cells(driver,table_header_list,missing_list,student_output_row) and has_all_selections

            # Extract rubric grade.
            rubric_calculated_points = extract_rubric_grade(driver)
            student_output_row.append(str(current_exam_points))
            student_output_row.append(str(rubric_calculated_points))
            # Exit rubric page.
            exit_button = driver.find_element(By.CSS_SELECTOR, 'div.taskbuttondiv_wrapper > p.taskbuttondiv[id="bottom_submitButtonRow"] > input[name="bottom_Exit"]')
            scroll_into_view_then_click(driver, exit_button)
        # Switch back to main window.
        driver.switch_to.window(main_window)
    # Return whether or not the rubric is fully completed.
    return (has_all_selections,missing_list)

def main():
    if len(sys.argv) != 4:
        print('usage: python3 driver.py chromedriver grading_url rubric_output_file')
        print('  chromedriver: path to chromedriver executable')
        print('  grading_url: UB Learns URL to Course Tools > Grade Center > Tests. ')
        print('  rubric_output_file: /path/to/output.csv')
        sys.exit()

    # sys.argv[1] -- path to chromedriver.
    pathToChromedriver = sys.argv[1]
    # sys.argv[2] -- gradebook url.
    grading_url = sys.argv[2]
    # sys.argv[3] -- output_file_path.
    output_file_path = sys.argv[3]

    #  Load Chromedriver in incognito mode (so nothing is saved)
    options = webdriver.ChromeOptions()
    options.add_argument('--incognito')
    driver = webdriver.Chrome(pathToChromedriver, 0, options)

    delay_amount = 1
    # Load Autolab
    driver.get('https://ublearns.buffalo.edu/')

    # Click login
    loginButton = driver.find_element_by_class_name('login')
    loginButton.click()

    # input('After logging into UB Learns and Loading Grading Page on Chrome webdriver, press enter...')

    wait = WebDriverWait(driver, 180)
    try:
        wait.until(EC.title_is("Help and Updates"))
    except:
        print('Took too long to authenticate/login to UB Learns.')
        sys.exit(1)

    # Collect rubrics from current problem.
    main_window = driver.current_window_handle
    # Use switch_to.window(handle) to switch windows.
    # driver.switch_to.window(main_window);

    msg = ''

    # Move to grading page and request user to open attempts.
    driver.get(grading_url)

    input('Open "Grade Attempts" for the exam to check, navigate to the first student, and then press enter...')
    previous_index = ''
    previous_name = ''
    current_index = ''
    final_index = ''
    output_rows = []
    while msg.lower() != 'n':
        try:
            # Switch to iframe holding grading panel.
            # iframe_element = driver.find_element_by_class_name('classic-learn-iframe')
            # driver.switch_to.frame('classic-learn-iframe')
            index_element = driver.find_element(By.CSS_SELECTOR, 'div.user-navigator > div.students-pager > span.count')
            index_text = index_element.text
            current_index = index_text[8:index_text.find(' ', 8)]
            if final_index == '':
                index_text = index_text[index_text.find('of') + 3:]
                final_index = index_text[:index_text.find(' ')]

            name_element = driver.find_element(By.CSS_SELECTOR, 'div.user-navigator > div.students-pager > h3[id]')
            student_name = name_element.text
            attempt_text = student_name[student_name.rfind('(') + 9:student_name.rfind(')')]
            student_name = student_name[:student_name.rfind('(')].strip()
            attempt_index = attempt_text[:attempt_text.find(' ')]
            latest_submission_index = attempt_text[attempt_text.rfind(' ') + 1:]
            student_output_row = [name_element.text]
            # Only check latest submission for progress.
            if attempt_index == latest_submission_index:
                # Check feedback in rubric.
                has_all_selections, missing_list = open_and_check_rubrics(driver, main_window, delay_amount,student_output_row)
                if not has_all_selections:
                    # Print missing entries for student.
                    print(student_name, missing_list)
                # end if
            # end if
            output_rows.append(student_output_row)

            if current_index == final_index:
                break

            # Move to next.
            next_page_element = driver.find_element(By.CSS_SELECTOR,'div.user-navigator > div.pager.next > a[title="Grade Next Item"]')
            next_page_element.click()

            previous_index = current_index
            previous_name = student_name
            # Wait for next submission to reload.
            WebDriverWait(driver,30).until(EC.staleness_of(next_page_element))
            time.sleep(0.5)
            WebDriverWait(driver,30).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'span.stepTitleRight.liveArea > p.taskbuttondiv > input.genericButton')))
        except Exception as err:
            # exc_type, exc_value, exc_traceback = sys.exc_info()
            # print('Driver window title',driver.title)
            # print("*** print_tb:")
            # traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
            # print("*** print_exception:")
            # # exc_type below is ignored on 3.5 and later
            # traceback.print_exception(exc_type, exc_value, exc_traceback,
            #                           limit=2, file=sys.stdout)
            # print("*** print_exc:")
            # traceback.print_exc(limit=2, file=sys.stdout)
            # print("*** format_exc, first and last line:")
            # formatted_lines = traceback.format_exc().splitlines()
            # print(formatted_lines[0])
            # print(formatted_lines[-1])
            # print("*** format_exception:")
            # # exc_type below is ignored on 3.5 and later
            # print(repr(traceback.format_exception(exc_type, exc_value,
            #                                       exc_traceback)))
            # print("*** extract_tb:")
            # print(repr(traceback.extract_tb(exc_traceback)))
            # print("*** format_tb:")
            # print(repr(traceback.format_tb(exc_traceback)))
            # print("*** tb_lineno:", exc_traceback.tb_lineno)
            # time.sleep(delay_amount)
            # Print error that was raised.
            print('Error:\n-----')
            print(err)
            print('-----')

            # Close any open windows aside from main window.
            for open_window in driver.window_handles:
                if open_window != main_window:
                    driver.switch_to.window(open_window)
                    driver.close()
            driver.switch_to.window(main_window)
            msg = input(f'UB Learns kicked back to gradebook (most likely). Is it fixed?\nLast index: {previous_index}. Last student processed:{previous_name}.\nPress enter to continue when ready to proceed...')
        # msg = input('Continue? (Y/N): ')

    # Close browser
    driver.close()

    with open(output_file_path,'w', newline='') as csv_file:
        spamwriter = csv.writer(csv_file)
        spamwriter.writerow(['Student Name'])
        spamwriter.writerows(output_rows)

if __name__ == '__main__':
    main()
