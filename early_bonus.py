from datetime import date, timedelta
import csv
from selenium.webdriver.common.by import By


def convert_date(date_string, deadline_hour):
    #print('Date string:',date_string)
    first_dash_index = date_string.find('-')
    second_dash_index = date_string.find('-', first_dash_index + 1)
    space_index = date_string.find(' ')
    colon_index = date_string.find(':')
    year = int(date_string[0:first_dash_index])
    month = int(date_string[first_dash_index + 1:second_dash_index])
    day = int(date_string[second_dash_index + 1:space_index])
    hour = int(date_string[space_index+1:colon_index])

    # Add one day based on 24-hour period from submission and not calendar day.
    if hour >= deadline_hour:
        day_offset = timedelta(1)
    else:
        day_offset = timedelta(0)

    return date(year=year, month=month, day=day) + day_offset
# End def


def early_bonus(driver, course, roster_emails, assignments, deadlines, hours, version_threshold):
    student_submission_data = {}
    assignment_deadline_map = {}

    for assignment, deadline, deadline_hour in zip(assignments,deadlines,hours):
        if assignment[:3] not in student_submission_data:
            student_submission_data[assignment[:3]] = {}
            assignment_deadline_map[assignment[:3]] = deadline
        students_submission_info = student_submission_data[assignment[:3]]
        # Load grade sheet.
        driver.get('https://autograder.cse.buffalo.edu/courses/' + course + '/assessments/' + assignment + '/viewGradesheet')

        # Scroll down the page to force the entire table to render.
        for i in range(0, 200):
            driver.execute_script("window.scrollTo(0,document.body.scrollHeight);")
        # End for

        # Propagate the hidden table info by clicking on all email addresses.
        latest_submission_index = len(driver.find_elements(By.CSS_SELECTOR, 'td.enumerator'))

        # Load popovers
        email_elements = driver.find_elements_by_class_name('email')
        # email_addresses = []
        for index, email_element in enumerate(email_elements):
            if index == latest_submission_index:
                break
            # End if
            driver.execute_script("arguments[0].scrollIntoView(true);", email_element)
            driver.execute_script("arguments[0].click();", email_element)

            # email_addresses.append(email_element.get_attribute('innerText').strip())
        # End for

        # Extract the page with all submissions from a single student and collect the links.
        submissions_table_rows = driver.find_element_by_id('grades').find_elements_by_class_name('sorting_1')
        for submissions_row in submissions_table_rows:
            email_address = str(submissions_row.find_element_by_class_name('email').get_attribute('innerText')).strip()
            # Skip students no longer in the course and TA/instructor submissions.
            if email_address not in roster_emails:
                continue
            # End if

            submission_info_element = submissions_row.find_elements_by_class_name('sub_info')[0]
            if version_threshold:
                # Adds "version over threshold by" at index 2 of this table.
                date_string = str(
                    submission_info_element.find_elements_by_tag_name('td')[3].get_attribute('innerText')).strip()
            else:
                date_string = str(
                    submission_info_element.find_elements_by_tag_name('td')[2].get_attribute('innerText')).strip()
            #End if

            if date_string == '0':
                # "Submission" is created from uploading grade and not a file.
                continue
            # End if

            submission_link = str(submission_info_element.find_elements_by_tag_name('a')[0].get_attribute('href')).strip()
            if email_address not in students_submission_info:
                students_submission_info[email_address] = {'submissionLinks': [submission_link],
                                                           'dates':[convert_date(date_string, deadline_hour)]}
            else:
                students_submission_info[email_address]['submissionLinks'].append(submission_link)
                students_submission_info[email_address]['dates'].append(convert_date(date_string, deadline_hour))
            # End if
        # End for
    # End for

    # Determine which submissions were made early (take latest of the submissions).
    for assignment, students_submission_info in student_submission_data.items():
        tweaked_students_log = []
        overcharged_students = []
        logdata = []
        deadline = assignment_deadline_map[assignment]
        for email_address, submissionInfo in students_submission_info.items():
            submission_date_list = students_submission_info[email_address]['dates']

            # Determine the latest date of submission.
            latest_submission_date = max(submission_date_list)
            # Count the number of late submissions.
            number_late = sum(map(lambda submission_date: submission_date > deadline, submission_date_list))

            if number_late > 1:
                # Student was charged multiple grace days for the same assignment.
                count = 0
                for index, submission_date in enumerate(submission_date_list):
                    if submission_date > deadline:
                        if submission_date < latest_submission_date or count > 0:
                            link_str = submissionInfo['submissionLinks'][index]
                            late_assignment = link_str[link_str.rfind('/', 0, link_str.rfind('/')) + 1:link_str.rfind('/')]
                            overcharged_students.append([email_address, late_assignment, str(submissionInfo['dates'][index])])
                        else:
                            count += 1
                        # End if
                    # End if
                # End for
            elif latest_submission_date < deadline:
                # Compute number of days the latest submission was early
                days_submitted_early = int((deadline - latest_submission_date).days)

                # # Give +1 point per day up to 5 days.
                # bonus = min(5, days_submitted_early)



                # Bonus will added in the tweak for the first assignment link.
                # 1. Navigate to the student's submissions page.
                submission_link = submissionInfo['submissionLinks'][0]
                # driver.get(submission_link)

                # Log the student bonus.
                tweaked_students_log.append([email_address, submission_link, str(latest_submission_date), days_submitted_early])

                # # 2. Obtain the rows in the submissions table.
                # student_submissions_table_rows = driver.find_element_by_class_name('highlight').find_element_by_tag_name('tbody').find_elements_by_tag_name('tr')
                #
                # # 3. Enter the tweak for the latest submission to include the bonus.
                # # 3a. Get the latest submission.
                # latest_submission_row = student_submissions_table_rows[0]
                #
                # # 3b. Find the edit link for the latest submission.
                # edit_link_element = latest_submission_row.find_elements_by_tag_name('a')[-1]
                #
                # if str(edit_link_element.get_attribute('innerText')).strip() == '(Edit)':
                #     # 3c. Click the edit link.
                #     edit_link_element.click()
                #
                #     # 3d. Find the HTML input element on the page to hold the tweak value.
                #     tweak_amount_input_element = driver.find_element_by_id('submission_tweak_attributes_value')
                #
                #     # 3e. Find the HTML input element on the page to hold the tweak notes.
                #     tweak_notes_input_element = driver.find_element_by_id('submission_notes')
                #
                #     # 3f. Log non-empty tweaks and adjust bonus.
                #     tweak_amount_str = str(tweak_amount_input_element.get_attribute('value')).strip()
                #     if tweak_amount_str:
                #         tweak_amount = int(float(tweak_amount_str))
                #         # # Ignore positive tweaks and only adjust if there was a deduction.
                #         # if tweak_amount < 0:
                #         bonus += tweak_amount
                #
                #         # Log previous and updated tweak amount.
                #         logdata.append([email_address, driver.current_url, tweak_amount_str, str(bonus)])
                #
                #     # 3g. Append to tweak notes that bonus being added.
                #     tweak_notes = str(tweak_notes_input_element.get_attribute('value')) + ' +' + str(bonus) + ' points for bonus.'
                #     tweak_notes_input_element.clear()
                #     tweak_notes_input_element.send_keys(tweak_notes)
                #
                #     # 3h. Update tweak amount.
                #     tweak_amount_input_element.clear()
                #     tweak_amount_input_element.send_keys(str(bonus))
                #
                #     # 3i. Commit changes by clicking the commit button.
                #     driver.find_elements_by_name('commit')[0].click()
                #
                #     # 3j. Log student that was adjusted.
                #     tweaked_students_log.append([email_address, submission_link, str(latest_submission_date)])
                # else:
                #     print('Error getting edit link for ' + email_address)
                # # End if
            # End if
        # End for

        # Write csv file containing modified students and also double charged students.
        with open(assignment + '-early-late-data.csv', 'w', newline='', encoding='ascii') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

            spamwriter.writerow(['Early Students'])
            spamwriter.writerow(['Email', 'Submission Link', 'Latest Submission', 'Days Submitted Early'])
            for outputArray in tweaked_students_log:
                spamwriter.writerow(outputArray)

            spamwriter.writerow(['-----', '-----', '-----'])
            spamwriter.writerow(['-----', '-----', '-----'])
            spamwriter.writerow(['Late Students'])
            spamwriter.writerow(['Email', 'Assignment', 'Latest Submission'])

            for outputArray in overcharged_students:
                spamwriter.writerow(outputArray)

            spamwriter.writerow(['-----', '-----', '-----'])
            spamwriter.writerow(['-----', '-----', '-----'])
            spamwriter.writerow(['Non-empty Tweak Data'])
            spamwriter.writerow(['Email', 'Submission Link', 'Original Tweak', 'Updated Tweak'])

            for outputArray in logdata:
                spamwriter.writerow(outputArray)

        # print('Modified students:',tweaked_students_log,sep='\n')
        # print('Overcharged students:', overcharged_students, sep='\n')
# End def


def zero_tweaks(driver, course, roster_emails, assignments, deadline, deadline_hour, version_threshold):
    students_submission_info = {}
    tweaked_students = []
    overcharged_students = []

    for assignment in assignments:
        # Load grade sheet.
        driver.get('https://autograder.cse.buffalo.edu/courses/' + course + '/assessments/' + assignment + '/viewGradesheet')

        # Scroll down the page to force the entire table to render.
        for i in range(0, 200):
            driver.execute_script("window.scrollTo(0,document.body.scrollHeight);")
        # End for

        # Propagate the hidden table info by clicking on all email addresses.
        submissions_table_rows = driver.find_element_by_id('grades').find_elements_by_class_name('sorting_1')
        for submissions_row in submissions_table_rows:
            element = submissions_row.find_elements_by_tag_name('a')[0]
            driver.execute_script("arguments[0].scrollIntoView(true);", element)
            element.click()
        # End for

        # Extract the page with all submissions from a single student and collect the links.
        submission_links = []
        submissions_table_rows = driver.find_element_by_id('grades').find_elements_by_class_name('sorting_1')
        for submissions_row in submissions_table_rows:
            email_address = str(submissions_row.find_element_by_class_name('email').get_attribute('innerText')).strip()
            # Skip students no longer in the course and TA/instructor submissions.
            if email_address not in roster_emails:
                continue
            # End if

            submission_info_element = submissions_row.find_elements_by_class_name('sub_info')[0]

            submission_link = str(submission_info_element.find_elements_by_tag_name('a')[0].get_attribute('href')).strip()
            submission_links.append(submission_link)

        for submission_link in submission_links:
            # Bonus will go onto the second assignment unless only one is made.
            driver.get(submission_link)

            # Get the rows in the submissions table.
            student_submissions_table_rows =\
                driver.find_element_by_class_name('highlight')\
                .find_element_by_tag_name('tbody')\
                .find_elements_by_tag_name('tr')

            # Enter the tweak for the latest submission to include the bonus.
            latest_submission_row = student_submissions_table_rows[0]

            # Find the edit link for the latest submission.
            edit_link_element = latest_submission_row.find_elements_by_tag_name('a')[-1]
            if str(edit_link_element.get_attribute('innerText')).strip() == '(Edit)':
                # Click the edit link.
                edit_link_element.click()

                # Find the HTML input element on the page to hold the tweak value.
                tweak_amount_input_element = driver.find_element_by_id('submission_tweak_attributes_value')

                tweak_value_str = tweak_amount_input_element.get_attribute('value')
                # If there is a tweak value, set it to 0.
                # (cannot set non-empty tweak value to empty or Autolab will ignore it).
                if tweak_value_str.strip():
                    # Clear any previous value.
                    tweak_amount_input_element.clear()
                    # Adjust the score by the bonus amount (1 point per day early).
                    tweak_amount_input_element.send_keys(str(0))

                # Commit changes by clicking the commit button.
                # driver.find_elements_by_name('commit')[0].click()

                # Note student that was adjusted.
                tweaked_students.append((email_address, submission_link))
            else:
                print('Error getting edit link for ' + email_address)
            # End if
        # End for
    # End for

    # Write csv file containing modified students and also double charged students.
    with open(assignment + '-cleared-tweak-data.csv', 'w', newline='', encoding='ascii') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

        spamwriter.writerow(['Email', 'Submission Link', 'Latest Submission'])
        for (email_address,link) in tweaked_students:
            outputArray = [email_address,link]
            spamwriter.writerow(outputArray)

        spamwriter.writerow(['-----', '-----', '-----'])
        spamwriter.writerow(['-----', '-----', '-----'])
        spamwriter.writerow(['Late Students'])
        spamwriter.writerow(['Email', 'Submission Link', 'Latest Submission'])

        for (email_address,link,latest_submission_date) in overcharged_students:
            outputArray = [email_address,link,latest_submission_date]
            spamwriter.writerow(outputArray)

    # print('Modified students:',tweaked_students,sep='\n')
    # print('Overcharged students:', overcharged_students, sep='\n')
# End def