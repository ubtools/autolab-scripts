from selenium import webdriver
from selenium.webdriver.common.by import By
import csv
import time
import traceback
import sys

def scroll_into_view_then_click(driver,element):
    driver.execute_script("arguments[0].scrollIntoView(true);", element)
    driver.execute_script("arguments[0].click();", element)

def open_and_collect_rubric_selections(driver, delay_amount, rubric_selections):
    # document.querySelectorAll('#selectedGraderRubric > input.inlineButton.rubric')
    current_grader_rubric_button = driver.find_element(By.XPATH, '//*[@id="selectedGraderRubric"]/input[1]')
    # Open grader1 rubric.
    scroll_into_view_then_click(driver, current_grader_rubric_button)
    time.sleep(delay_amount)
    # Move the driver to new window.
    rubric_window = driver.window_handles[1]
    driver.switch_to.window(rubric_window)
    # Extract grader feedback.
    table_row_list = driver.find_elements(By.XPATH, '//tr[@class="rubricGradingRow"]')
    table_header_list = driver.find_elements(By.XPATH, '//tr[@class="rubricGradingRow"]/th')

    for r, table_row, problem_header in zip(range(0,len(table_row_list)),table_row_list, table_header_list):
        table_cells = table_row.find_elements(By.TAG_NAME,'td')
        if len([i for i in range(0, 4) if 'selectedCell' in table_cells[i].get_attribute('class')]) > 0:
            selected = min(i for i in range(0, 4) if 'selectedCell' in table_cells[i].get_attribute('class'))
            rubric_selections[r] = selected
            # print(problem_header.text, ':', table_cells[selected].text)

    exit_button = driver.find_element(By.XPATH, '//*[@id="bottom_submitButtonRow"]/input')
    scroll_into_view_then_click(driver, exit_button)

def open_and_fill_rubric(driver, delay_amount, rubric_selections):
    # Open solidified rubric.
    # document.querySelectorAll('.genericButtonImg')
    attempt_score_input = driver.find_element(By.XPATH, '//*[@id="currentAttempt_grade"]')
    # Open student feedback.
    scroll_into_view_then_click(driver, attempt_score_input)
    time.sleep(delay_amount)
    # Overall grading rubric.
    overall_rubric_button = driver.find_element(By.XPATH, '//*[@id="collabRubricList"]/div/div[2]/a')
    scroll_into_view_then_click(driver, overall_rubric_button)
    time.sleep(delay_amount)
    # Move the driver to new window.
    rubric_window = driver.window_handles[1]
    driver.switch_to.window(rubric_window)
    # Extract grader feedback.
    table_row_list = driver.find_elements(By.XPATH, '//tr[@class="rubricGradingRow"]')
    table_header_list = driver.find_elements(By.XPATH, '//tr[@class="rubricGradingRow"]/th')

    for selection, table_row, problem_header in zip(rubric_selections, table_row_list, table_header_list):
        table_cells = table_row.find_elements(By.TAG_NAME, 'td')
        scroll_into_view_then_click(driver,table_cells[selection])

    save_button = driver.find_element(By.XPATH, '//*[@id="bottom_submitButtonRow"]/input[2]')
    scroll_into_view_then_click(driver, save_button)

if len(sys.argv) != 3:
    print('usage: python3 driver.py chromedriver roster')
    print('  course: name of course on autolab. E.g., cse250-f18')
    print('  chromedriver: path to chromedriver executable')
    print('  roster: path to course roster csv file. Can be downloaded from UBLearns using comma separator.')
    print('          expected format: "Last Name","First Name","Username","Student ID","Last Access","Availability"')
    print('  assignments: comma separated list of assignments (no spaces)')
    print('          assignment string can be obtained from url for assignment in Autolab')
    print('          E.g., https://autograder.cse.buffalo.edu/courses/cse250-f18/assessments/a1programming')
    print('                has assignment string a1programming')
    sys.exit()

# sys.argv[1] -- Autolab course.
#course = sys.argv[1]

# sys.argv[2] -- path to chromedriver.
# pathToChromedriver = sys.argv[2]
pathToChromedriver = sys.argv[1]
#  Load Chromedriver in incognito mode (so nothing is saved)
options = webdriver.ChromeOptions()
options.add_argument('--incognito')
driver = webdriver.Chrome(pathToChromedriver,0,options)

delay_amount = 1
# Load Autolab
driver.get('https://ublearns.buffalo.edu/')

# Click login
loginButton = driver.find_element_by_class_name('login')
loginButton.click()

# Process roster while waiting for user to enter login credentials

# sys.argv[3] -- roster path
roster_path = sys.argv[2]

person_number_to_ubit_map = {}
ubit_map_to_person_number = {}
name_map_to_ubit = {}
ubit_map_to_row_info = {}
roster_emails = []
with open(roster_path,newline='') as roster_file:
    reader = csv.reader(roster_file, delimiter=',', quoting=csv.QUOTE_MINIMAL)

    # Skip header row.
    next(reader,None)

    for row in reader:
        last_name = str(row[0]).strip()
        first_name = str(row[1]).strip()
        ubit = str(row[2]).strip()
        person_number = str(row[3]).strip()
        # Map person number to ubit name.
        person_number_to_ubit_map[person_number] = ubit

        # Map name to ubit.
        name_map_to_ubit[first_name + ' ' + last_name] = ubit

        # Map ubit name to person number.
        ubit_map_to_person_number[ubit] = person_number

        # Map ubit name to person number.
        ubit_map_to_row_info[ubit] = [last_name,first_name,ubit,person_number]

        # Create emails from roster.
        roster_emails.append(ubit+'@buffalo.edu')

print('Loaded', len(roster_emails),'students.')

input('After logging into UB Learns and Loading Grading Page on Chrome webdriver, press enter...')

# Collect rubrics from current problem.
main_window = driver.current_window_handle

# Use switch_to.window(handle) to switch windows.
# driver.switch_to.window(main_window);

msg = ''

driver.get('https://ublearns.blackboard.com/webapps/assessment/do/gradeTest?outcomeDefinitionId=_1428541_1&currentAttemptIndex=1&numAttempts=193&anonymousMode=false&sequenceId=_172931_1_0&course_id=_172931_1&source=cp_gradebook&viewInfo=Tests&attempt_id=_34898772_1&courseMembershipId=_9379799_1&cancelGradeUrl=%2Fwebapps%2Fgradebook%2Fdo%2Finstructor%2FenterGradeCenter%3Fcourse_id%3D_172931_1&submitGradeUrl=%2Fwebapps%2Fgradebook%2Fdo%2Finstructor%2FperformGrading%3Fcourse_id%3D_172931_1%26sequenceId%3D_172931_1_0%26cmd%3Dnext')

i = 1

while i <= number_of_submissions:
    try:
        # Switch to iframe holding grading panel.
        # iframe_element = driver.find_element_by_class_name('classic-learn-iframe')
        # driver.switch_to.frame('classic-learn-iframe')
        rubric_rows = 17
        rubric_selections = [-1 for i in range(0, rubric_rows)]

        # Process grader1 rubric.
        open_and_collect_rubric_selections(driver, delay_amount, rubric_selections)
        driver.switch_to.window(main_window)

        # driver.switch_to.window(main_window)

        # Switch to grader 2 and process grader2 rubric.
        # document.querySelector('#currentAttempt_gradersListButton')
        grader_list_element = driver.find_element(By.XPATH, '//*[@id="currentAttempt_gradersListButton"]')
        scroll_into_view_then_click(driver,grader_list_element)
        time.sleep(delay_amount)
        # document.querySelector('#graderName_2').parentNode
        grader_list_element_other_grader = driver.find_element(By.XPATH,'//*[@id="currentAttempt_gradersList"]/ul/li[2]/a')
        scroll_into_view_then_click(driver,grader_list_element_other_grader)
        time.sleep(delay_amount)
        open_and_collect_rubric_selections(driver, delay_amount, rubric_selections)
        driver.switch_to.window(main_window)

        # Set feedback in solidified rubric.
        open_and_fill_rubric(driver, delay_amount, rubric_selections)
        driver.switch_to.window(main_window)
        # Submit button
        submit_element = driver.find_element(By.XPATH,'//*[@id="currentAttempt_submitButton"]')
        msg = input('Submit? (Y/N): ')
        if msg.lower() == 'y':
            scroll_into_view_then_click(driver, submit_element)
            time.sleep(2*delay_amount)

    except Exception as err:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print('Driver window title',driver.title)
        print("*** print_tb:")
        traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
        print("*** print_exception:")
        # exc_type below is ignored on 3.5 and later
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                                  limit=2, file=sys.stdout)
        print("*** print_exc:")
        traceback.print_exc(limit=2, file=sys.stdout)
        print("*** format_exc, first and last line:")
        formatted_lines = traceback.format_exc().splitlines()
        print(formatted_lines[0])
        print(formatted_lines[-1])
        print("*** format_exception:")
        # exc_type below is ignored on 3.5 and later
        print(repr(traceback.format_exception(exc_type, exc_value,
                                              exc_traceback)))
        print("*** extract_tb:")
        print(repr(traceback.extract_tb(exc_traceback)))
        print("*** format_tb:")
        print(repr(traceback.format_tb(exc_traceback)))
        print("*** tb_lineno:", exc_traceback.tb_lineno)

    # msg = input('Continue? (Y/N): ')


# Close browser
driver.close()
