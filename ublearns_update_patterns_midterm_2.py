from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import traceback
import sys

def convert_list_to_seq_pattern_string(l):
    seq_string = '\\s*\\(\\s*' + str(l)[1:-1].replace(',','\\s*,\\s*') + '\\s*\\)'
    return seq_string

def scroll_into_view_then_click(driver,element):
    driver.execute_script("arguments[0].scrollIntoView(true);", element)
    driver.execute_script("arguments[0].click();", element)

def open_and_check_rubric(driver, delay_amount):
    # Open solidified rubric.
    # document.querySelectorAll('.genericButtonImg')
    attempt_score_input = driver.find_element(By.XPATH, '//*[@id="currentAttempt_grade"]')
    # Open student feedback.
    scroll_into_view_then_click(driver,driver, attempt_score_input)
    time.sleep(delay_amount)
    # Overall grading rubric.
    overall_rubric_button = driver.find_element(By.XPATH, '//*[@id="collabRubricList"]/div/div[2]/a')
    scroll_into_view_then_click(driver,driver, overall_rubric_button)
    time.sleep(delay_amount)
    # Move the driver to new window.
    rubric_window = driver.window_handles[1]
    driver.switch_to.window(rubric_window)
    # Extract grader feedback.
    selected_cell_count = len(driver.find_elements(By.XPATH, '//tr[@class="rubricGradingRow"]/td[@class="rubricGradingCell selectedCell"]'))
    table_header_list = driver.find_elements(By.XPATH, '//tr[@class="rubricGradingRow"]/th')
    has_all_selections = table_header_list and (selected_cell_count == len(table_header_list))
    missing_list = []
    if not has_all_selections:
        table_rows = driver.find_elements(By.XPATH, '//tr[@class="rubricGradingRow"]')
        for i,row in enumerate(table_rows):
            if len(row.find_elements(By.CLASS_NAME,'selectedCell')) == 0:
                missing_list.append(table_header_list[i].text)

    exit_button = driver.find_element(By.XPATH, '//*[@id="bottom_submitButtonRow"]/input[@name="bottom_Exit"]')
    scroll_into_view_then_click(driver,driver, exit_button)
    # Return whether or not the rubric is fully completed.
    return (has_all_selections,missing_list)

if len(sys.argv) != 2:
    print('usage: python3 driver.py chromedriver roster')
    print('  course: name of course on autolab. E.g., cse250-f18')
    print('  chromedriver: path to chromedriver executable')
    print('  roster: path to course roster csv file. Can be downloaded from UBLearns using comma separator.')
    print('          expected format: "Last Name","First Name","Username","Student ID","Last Access","Availability"')
    print('  assignments: comma separated list of assignments (no spaces)')
    print('          assignment string can be obtained from url for assignment in Autolab')
    print('          E.g., https://autograder.cse.buffalo.edu/courses/cse250-f18/assessments/a1programming')
    print('                has assignment string a1programming')
    sys.exit()

# sys.argv[1] -- Autolab course.
#course = sys.argv[1]

# sys.argv[2] -- path to chromedriver.
# pathToChromedriver = sys.argv[2]
pathToChromedriver = sys.argv[1]
#  Load Chromedriver in incognito mode (so nothing is saved)
options = webdriver.ChromeOptions()
options.add_argument('--incognito')
driver = webdriver.Chrome(pathToChromedriver,0,options)

delay_amount = 1
# Load Autolab
driver.get('https://ublearns.buffalo.edu/')

# Click login
loginButton = driver.find_element_by_class_name('login')
loginButton.click()

wait = WebDriverWait(driver, 180)
try:
    wait.until(EC.title_is("Help and Updates"))
except:
    print('Took too long to authenticate/login to Autolab.')
    sys.exit(1)

# Collect rubrics from current problem.
main_window = driver.current_window_handle

# Use switch_to.window(handle) to switch windows.
# driver.switch_to.window(main_window);

msg = ''

driver.get('https://ublearns.blackboard.com/webapps/blackboard/landingPage.jsp?navItem=cp_test_survey_pool&course_id=_175126_1&sortItems=false')

input('Open the page for the Midterm 2 Sel Sort Pool and then press enter...')
current_index = 0
pause_console = True
while msg.lower() != 'n':
    try:
        # Switch to iframe holding grading panel.
        iframe_element = driver.find_elements_by_class_name('classic-learn-iframe')
        if iframe_element:
            driver.switch_to.frame('classic-learn-iframe')
        # From Pool page, click dropdown element.
        dropdown_elements = driver.find_elements(By.CSS_SELECTOR, 'table#pool_listContainer_datatable > tbody#pool_listContainer_databody > tr > th > span > a')
        action = ActionChains(driver)
        action.move_to_element(dropdown_elements[current_index]).pause(0.25).click().perform()
        # Click Edit on pop-over window.
        edit_element = driver.find_element(By.CSS_SELECTOR, 'div.cmdiv > ul > li > a[title="Edit"]')
        scroll_into_view_then_click(driver,edit_element)
        # Wait for question page to load (takes some time).
        WebDriverWait(driver,10).until(EC.presence_of_element_located((By.CSS_SELECTOR,"li.required")))
        # Click Next on Question Edit Page to load answers.
        driver.switch_to.window(main_window)
        next_element = driver.find_element(By.CSS_SELECTOR, 'p#bottom_submitButtonRow > input[name="bottom_Next"]')
        scroll_into_view_then_click(driver,next_element)
        # Get list of textarea elements.
        textarea_elements = WebDriverWait(driver,10).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR,"tr.answerBlock > td > textarea")))
        if len(textarea_elements) != 8 and len(textarea_elements) != 4 and len(textarea_elements) != 6: raise Exception("Insufficient number of text areas found.")
        if len(textarea_elements) == 8:
            for i in [3,5,7]:
                exact_answer_text = textarea_elements[i-1].text
                regex_pattern_text = exact_answer_text.replace('(','\\s*[({]?\\s*').replace(')','\\s*[)}]?\\s*').replace(',','\\s*[,.]?\\s*')
                regex_answer_element = textarea_elements[i]
                regex_answer_element.clear()
                regex_answer_element.send_keys(regex_pattern_text)
        elif len(textarea_elements) == 4:
            for i in [1]:
                exact_answer_text = textarea_elements[i - 1].text
                regex_pattern_text = exact_answer_text.replace('(', '\\s*[({]?\\s*').replace(')','\\s*[)}]?\\s*').replace(',', '\\s*[,.]?\\s*')
                regex_answer_element = textarea_elements[i]
                regex_answer_element.clear()
                regex_answer_element.send_keys(regex_pattern_text)
            for i in [3]:
                regex_answer_element = textarea_elements[i]
                regex_answer_element.clear()
                regex_answer_element.send_keys('(?:[O0]|[tT]heta)\\s*\\(?\\s*n\\s*\\)?')

        elif len(textarea_elements) == 6:
            for i in [3,5]:
                exact_answer_text = textarea_elements[i - 1].text
                traversal_letters = exact_answer_text[1:-1].split(',')
                # regex_pattern_text = exact_answer_text.replace('(', '\\s*[({]?\\s*').replace(')','\\s*[)}]?\\s*').replace(',', '\\s*[,.]?\\s*')
                regex_pattern_text = '\\s*[({]?\\s*'
                for index,c in enumerate(traversal_letters):
                    regex_pattern_text += '[' + c.lower() + c.upper() + ']'
                    if index < len(traversal_letters) - 1:
                        regex_pattern_text += '\\s*[,.]?\\s*'
                    regex_pattern_text += '\\s*[)}]?\\s*'
                regex_answer_element = textarea_elements[i]
                regex_answer_element.clear()
                regex_answer_element.send_keys(regex_pattern_text)
        if pause_console:
            response = ''
            while response != 'Y' and response != 'STOP' and response != 'N':
                response = input('Continue? Y/N/STOP')
            if response == 'N':
                exit(0)
            elif response == 'STOP':
                pause_console = False
        # Click Next from answer page.
        next_element = driver.find_element(By.CSS_SELECTOR, 'p#bottom_submitButtonRow > input[name="bottom_Next"]')
        scroll_into_view_then_click(driver,next_element)
        time.sleep(delay_amount)
        # Click Save and Submit.
        save_element = driver.find_elements(By.CSS_SELECTOR, 'p#bottom_submitButtonRow > input[name="bottom_Submit and Update Attempts"]')
        if save_element:
            scroll_into_view_then_click(driver,save_element[0])
            driver.switch_to.alert.accept()
        else:
            save_element = driver.find_element(By.CSS_SELECTOR, 'p#bottom_submitButtonRow > input[name="bottom_Submit"]')
            scroll_into_view_then_click(driver, save_element)
        driver.switch_to.window(main_window)
        time.sleep(delay_amount)
        current_index += 1
    except Exception as err:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print('Driver window title',driver.title)
        print("*** print_tb:")
        traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
        print("*** print_exception:")
        # exc_type below is ignored on 3.5 and later
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                                  limit=2, file=sys.stdout)
        # print("*** print_exc:")
        # traceback.print_exc(limit=2, file=sys.stdout)
        # print("*** format_exc, first and last line:")
        # formatted_lines = traceback.format_exc().splitlines()
        # print(formatted_lines[0])
        # print(formatted_lines[-1])
        # print("*** format_exception:")
        # # exc_type below is ignored on 3.5 and later
        # print(repr(traceback.format_exception(exc_type, exc_value,
        #                                       exc_traceback)))
        # print("*** extract_tb:")
        # print(repr(traceback.extract_tb(exc_traceback)))
        # print("*** format_tb:")
        # print(repr(traceback.format_tb(exc_traceback)))
        # print("*** tb_lineno:", exc_traceback.tb_lineno)
        time.sleep(delay_amount)
        msg = input('Is it fixed? Last index: ' + str(current_index) + '. Press enter to continue...')
        if msg.lower() == 'reset':
            current_index = 0

    # msg = input('Continue? (Y/N): ')


# Close browser
driver.close()
