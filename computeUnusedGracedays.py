import csv
from collections import OrderedDict

from selenium.webdriver.common.by import By


def collect_gdu_pld(driver, course, roster_emails, assignments):
    gradebook_ids = {}
    for assignment in reversed(assignments):
        # Load grade sheet.
        driver.get(
            'https://autograder.cse.buffalo.edu/courses/' + course + '/assessments/' + assignment + '/viewGradesheet')

        # Scroll down the page to force the entire table to render.
        for i in range(0, 200):
            driver.execute_script("window.scrollTo(0,document.body.scrollHeight);")
        # End for

        # Propagate the hidden table info by clicking on all email addresses.
        latest_submission_index = len(driver.find_elements(By.CSS_SELECTOR, 'td.enumerator'))

        # Load popovers
        email_elements = driver.find_elements_by_class_name('email')
        # email_addresses = []
        for index, email_element in enumerate(email_elements):
            if index == latest_submission_index:
                break
            # End if
            driver.execute_script("arguments[0].click();", email_element)
            driver.execute_script("arguments[0].scrollIntoView(true);", email_element)

            # email_addresses.append(email_element.get_attribute('innerText').strip())
        # End for

        # Extract the page with all submissions from a single student and collect the links.
        submissions_table_rows = driver.find_elements_by_class_name('sub_info')
        filename_elements = driver.find_elements_by_class_name('filename')
        for index, submissions_row in enumerate(submissions_table_rows):
            email_address = filename_elements[index].get_attribute('innerText').strip()
            email_address = email_address[:email_address.find('_')]
            # Skip students no longer in the course and TA/instructor submissions.
            if email_address not in roster_emails or email_address in gradebook_ids:
                continue
            # if email_addresses[index] not in roster_emails or email_addresses[index] in gradebook_ids:
            #     continue
            # End if

            # Get link to student's submissions page.
            submission_link = str(submissions_row.find_element_by_tag_name('a').get_attribute('href')).strip()

            id_string = submission_link[submission_link.rfind('=') + 1:]

            # gradebook_ids[email_addresses[index]] = id_string
            gradebook_ids[email_address] = id_string
        # End for
    # End for
    del index
    del email_address
    del email_element
    del email_elements
    del submissions_row
    del submission_link
    del id_string
    del assignment
    del i
    del latest_submission_index
    del submissions_table_rows

    log_data = {}
    first_pass = True
    for email, gradebook_id in gradebook_ids.items():
        # Create output data dictionary.
        log_data[email] = OrderedDict()


        # Open gradebook link.
        # TODO: Generalize link.
        gradebook_link = 'https://autograder.cse.buffalo.edu/courses/' + course + '/course_user_data/16002/gradebook/student?id=' + gradebook_id
        driver.get(gradebook_link)

        # Collect the rows in the student submissions table.
        student_gradebook_tables = driver.find_elements(By.CSS_SELECTOR, 'table.grades')

        for table in student_gradebook_tables:
            header = True
            for table_row in table.find_elements(By.CSS_SELECTOR, 'tr')[:-1]:
                if header:
                    assignment_str = str(
                        table.find_elements(By.CSS_SELECTOR, 'th.cat_name')[0].get_attribute('innerText')).strip()
                    log_data[email][assignment_str] = OrderedDict()
                    header = False
                else:
                    row_values = table_row.find_elements(By.CSS_SELECTOR, 'td')
                    part_name = str(row_values[0].get_attribute('innerText')).strip()
                    part_gdu = str(row_values[1].get_attribute('innerText')).strip()
                    part_pld = str(row_values[2].get_attribute('innerText')).strip()
                    part_score = str(row_values[3].get_attribute('innerText')).strip()
                    log_data[email][assignment_str][part_name] = (part_gdu, part_pld, part_score)
                # End if
            # End for
        # End for
        if first_pass:
            first_row_header = ['']
            second_row_header = ['']
            third_row_header = ['Email']
            for assignment_str in log_data[email]:
                for part_name in log_data[email][assignment_str]:
                    for part_str in ['GDU', 'PDL', 'Score']:
                        first_row_header.append(assignment_str)
                        second_row_header.append(part_name)
                        third_row_header.append(part_str)
                # End for
            # End for
            first_pass = False
        # End if
    # End for

    # Write csv file containing modified students and also double charged students.
    with open(course + '-gradebook.csv', 'w', newline='', encoding='ascii') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        # Write header.
        spamwriter.writerow(first_row_header)
        spamwriter.writerow(second_row_header)
        spamwriter.writerow(third_row_header)

        # Write log data.
        for email in log_data:
            outputrow = [email]
            for assignment_str in log_data[email]:
                for part_name, (part_gdu, part_pld, part_score) in log_data[email][assignment_str].items():
                    outputrow.append(part_gdu)
                    outputrow.append(part_pld)
                    outputrow.append(part_score)
                # End for
            # End for
            spamwriter.writerow(outputrow)
        # End for
    # End with
# End def
