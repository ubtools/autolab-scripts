import os
import csv


def upload_files(driver, course, roster_emails, assignment):
    path = input('Enter the full path to the files to upload: ')
    prefix = input('Enter the upload filename prefix: ')
    if 'mapping.csv' in os.listdir(path):
        email_index_mapping = {}
        student_mapping_filename = path + '/mapping.csv'
        with open(student_mapping_filename, 'r') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
            for row in spamreader:
                if row:
                    email_index_mapping[row[0]] = row[1]
        del student_mapping_filename
        del row
        del spamreader
        del csvfile
    else:
        print('Requires file mapping.csv in', path, 'with headers: email,index')
        return

    for student_email in roster_emails:
        # Get full path of submission pdf.
        if student_email not in email_index_mapping:
            continue
        file_number = email_index_mapping[student_email].zfill(3)
        filename = path + prefix + file_number + '.pdf'
        student_submission_fullpath = os.path.abspath(filename)

        # Submissions Page
        # Go to page to create a new submission for the assignment.
        driver.get(
            'https://autograder.cse.buffalo.edu/courses/' + course + '/assessments/' + assignment + '/submissions/new')

        # Get the user list dropdown element.
        users_dropdown_input = driver.find_elements_by_class_name("select-dropdown")[0]

        # Get the user list.
        users_dropdown_list = driver.find_elements_by_class_name("select-dropdown")[1]
        users_list_items = users_dropdown_list.find_elements_by_tag_name("li")

        # Open the user list dropdown.
        users_dropdown_input.click()

        # Find the entry in the list corresponding to the upload.
        for item in users_list_items:
            # print(item)
            text = str(item.get_attribute('innerText'))
            if student_email in text:
                # When the user is found, click the entry in the list for the user we want to upload to.
                driver.execute_script("arguments[0].scrollIntoView(true);", item)
                item.click()
                break

        # Set the file upload dialog to take the file submission for the student.
        file_input = driver.find_element_by_id('submission_file')
        file_input.send_keys(student_submission_fullpath)

        # Click the submit button.
        submit_button = driver.find_element_by_name('commit')
        submit_button.click()
