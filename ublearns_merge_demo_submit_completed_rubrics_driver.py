from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import sys
import traceback
import csv

def addNumbers(x1, x2):
    return x1 + x2

def scroll_into_view_then_click(driver,element):
    driver.execute_script("arguments[0].scrollIntoView(true);", element)
    driver.execute_script("arguments[0].click();", element)

def fill_demo_data(driver, delay_amount, demo_data):
    # Extract grader feedback.
    table_row_list = driver.find_elements(By.XPATH, '//tr[@class="rubricGradingRow"]')[-len(demo_data):]
    table_header_list = driver.find_elements(By.XPATH, '//tr[@class="rubricGradingRow"]/th')[-len(demo_data):]

    for selection, table_row, problem_header in zip(demo_data, table_row_list, table_header_list):
        table_cells = table_row.find_elements(By.TAG_NAME, 'td')
        if selection:
            scroll_into_view_then_click(driver, table_cells[0])
        else:
            scroll_into_view_then_click(driver, table_cells[1])
        #print(selection,problem_header,table_row)

def open_and_check_rubric(driver, delay_amount, demo_data):
    # Open solidified rubric.
    # document.querySelectorAll('.genericButtonImg')
    attempt_score_input = driver.find_element(By.XPATH, '//*[@id="currentAttempt_grade"]')
    # Open student feedback.
    scroll_into_view_then_click(driver, attempt_score_input)
    time.sleep(delay_amount)
    # Overall grading rubric.
    overall_rubric_button = driver.find_element(By.XPATH, '//*[@id="collabRubricList"]/div/div[2]/a')
    scroll_into_view_then_click(driver, overall_rubric_button)
    time.sleep(delay_amount)
    # Move the driver to new window.
    rubric_window = driver.window_handles[1]
    driver.switch_to.window(rubric_window)
    # Fill in demo data.
    fill_demo_data(driver,delay_amount,demo_data)
    # Extract grader feedback.
    selected_cell_count = len(driver.find_elements(By.XPATH, '//tr[@class="rubricGradingRow"]/td[@class="rubricGradingCell selectedCell"]'))
    table_header_list = driver.find_elements(By.XPATH, '//tr[@class="rubricGradingRow"]/th')
    has_all_selections = table_header_list and (selected_cell_count == len(table_header_list))
    missing_list = []
    if not has_all_selections:
        table_rows = driver.find_elements(By.XPATH, '//tr[@class="rubricGradingRow"]')
        for i,row in enumerate(table_rows):
            if len(row.find_elements(By.CLASS_NAME,'selectedCell')) == 0:
                missing_list.append(table_header_list[i].text)

    # exit_button = driver.find_element(By.XPATH, '//*[@id="bottom_submitButtonRow"]/input[@name="bottom_Exit"]')
    # scroll_into_view_then_click(driver, exit_button)
    save_button = driver.find_element(By.XPATH, '//*[@id="bottom_submitButtonRow"]/input[@name="bottom_Save"]')
    scroll_into_view_then_click(driver, save_button)
    # Return whether or not the rubric is fully completed.
    return (has_all_selections,missing_list)

def main():
    if len(sys.argv) != 4:
        print('usage: python3 driver.py chromedriver grading_url demo_scores.csv')
        print('  chromedriver: path to chromedriver executable')
        print('  grading_url: UB Learns URL to Course Tools > Grade Center > Assignments. ')
        sys.exit()

    # sys.argv[1] -- path to chromedriver.
    pathToChromedriver = sys.argv[1]
    # sys.argv[2] -- gradebook url.
    grading_url = sys.argv[2]
    # sys.argv[3] -- demo scores csv.
    demo_scores_csv = sys.argv[3]

    #  Load Chromedriver in incognito mode (so nothing is saved)
    options = webdriver.ChromeOptions()
    options.add_argument('--incognito')
    driver = webdriver.Chrome(pathToChromedriver, 0, options)

    delay_amount = 1
    # Load Autolab
    driver.get('https://ublearns.buffalo.edu/')

    # Click login
    loginButton = driver.find_element_by_class_name('login')
    loginButton.click()

    roster_data = {}
    with open(demo_scores_csv, newline='') as roster_file:
        reader = csv.reader(roster_file, delimiter=',', quoting=csv.QUOTE_MINIMAL)

        # Skip header row.
        next(reader, None)

        for row in reader:
            # Map person number to ubit name.
            student_name = str(row[1]) + ' ' + str(row[0])

            student_data = list(map(lambda x : x == "TRUE" ,row[5:]))
            roster_data[student_name] = student_data

    print('Loaded data for ', len(roster_data), 'students.')

    # input('After logging into UB Learns and Loading Grading Page on Chrome webdriver, press enter...')

    wait = WebDriverWait(driver, 180)
    try:
        wait.until(EC.title_is("Help and Updates"))
    except:
        print('Took too long to authenticate/login to UB Learns.')
        sys.exit(1)

    # Collect rubrics from current problem.
    main_window = driver.current_window_handle
    # Use switch_to.window(handle) to switch windows.
    # driver.switch_to.window(main_window);

    msg = ''

    # Move to grading page and request user to open attempts.
    driver.get(grading_url)

    input('Set filter for assignment to check and then press enter...')
    previous_index = ''
    previous_name = ''
    current_index = ''
    final_index = ''
    while msg.lower() != 'n':
        try:
            # Switch to iframe holding grading panel.
            # iframe_element = driver.find_element_by_class_name('classic-learn-iframe')
            # driver.switch_to.frame('classic-learn-iframe')
            index_element = driver.find_element(By.CSS_SELECTOR, 'div.user-navigator > div.students-pager > span.count')
            index_text = index_element.text
            current_index = index_text[8:index_text.find(' ', 8)]
            if final_index == '':
                index_text = index_text[index_text.find('of') + 3:]
                final_index = index_text[:index_text.find(' ')]

            name_element = driver.find_element(By.CSS_SELECTOR, 'div.user-navigator > div.students-pager > h3[id]')
            student_name = name_element.text
            attempt_text = student_name[student_name.rfind('(') + 9:student_name.rfind(')')]
            student_name = student_name[:student_name.rfind('(')].strip()
            attempt_index = attempt_text[:attempt_text.find(' ')]
            latest_submission_index = attempt_text[attempt_text.rfind(' ') + 1:]
            # Only check latest submission for progress.
            if attempt_index == latest_submission_index:
                # Check feedback in rubric.
                has_all_selections,missing_list = open_and_check_rubric(driver, delay_amount,roster_data[student_name])
                driver.switch_to.window(main_window)
                if has_all_selections:
                    # Submit the completed rubric.
                    # Submit button
                    submit_element = driver.find_element(By.XPATH,'//*[@id="currentAttempt_submitButton"]')
                    # msg = input('Submit? (Y/N): ')
                    msg = 'y'
                    if msg.lower() == 'y':
                        scroll_into_view_then_click(driver, submit_element)
                        # time.sleep(2 * delay_amount)
                    # scroll_into_view_then_click(driver, submit_element)
                    pass
                else:
                    # Print missing entries for student.
                    print(student_name,missing_list)
                    # Move to next.
                    next_page_element = driver.find_element(By.XPATH, '//div[@class="pager next"]/a')
                    next_page_element.click()
                if current_index == final_index:
                    break
                previous_index = current_index
                previous_name = student_name
            else:
                # Move to next.
                next_page_element = driver.find_element(By.XPATH, '//div[@class="pager next"]/a')
                next_page_element.click()

            # Wait for next submission to reload.
            # WebDriverWait(driver, 30).until(EC.staleness_of(name_element))
            time.sleep(1.5)
            # WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'span.stepTitleRight.liveArea > p.taskbuttondiv > input.genericButton')))
        except Exception as err:
            # exc_type, exc_value, exc_traceback = sys.exc_info()
            # print('Driver window title',driver.title)
            # print("*** print_tb:")
            # traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
            # print("*** print_exception:")
            # # exc_type below is ignored on 3.5 and later
            # traceback.print_exception(exc_type, exc_value, exc_traceback,
            #                           limit=2, file=sys.stdout)
            # print("*** print_exc:")
            # traceback.print_exc(limit=2, file=sys.stdout)
            # print("*** format_exc, first and last line:")
            # formatted_lines = traceback.format_exc().splitlines()
            # print(formatted_lines[0])
            # print(formatted_lines[-1])
            # print("*** format_exception:")
            # # exc_type below is ignored on 3.5 and later
            # print(repr(traceback.format_exception(exc_type, exc_value,
            #                                       exc_traceback)))
            # print("*** extract_tb:")
            # print(repr(traceback.extract_tb(exc_traceback)))
            # print("*** format_tb:")
            # print(repr(traceback.format_tb(exc_traceback)))
            # print("*** tb_lineno:", exc_traceback.tb_lineno)
            msg = input(f'UB Learns kicked back to gradebook (most likely). Is it fixed?\nLast index: {previous_index}. Last student processed:{previous_name}.\nPress enter to continue when ready to proceed...')
        # msg = input('Continue? (Y/N): ')
    # Close browser
    driver.close()

if __name__ == "__main__":
    main()